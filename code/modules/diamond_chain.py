import kwant
import scipy
import numpy as np

def quartic_ti(N_c, system_params, orbitals = 's',semi_infinite=False):
    '''
    Create the quartic TI (square root TI of the diamond chain), following the prodecure outlined in arxiv:2102.12635 .
    The sites only have s orbitals (for now).
    It's like this, just tilted so that the [1,0] axis runs along the [1,-1] axis and the [0,1] axis along the [1,1] axis:
    
       b21 b22         b21 b22
         \ /             \ /
          B               B
         / \\            / \\
    ba  ab  ba          ab  ba 
     \\/     \\/     \\/     
      A   Φ   A  ...  A
     / \     / \     / \     
    ca  ac  ca          ac  ca
         \ /             \ /
          C               C
         / \             / \
       c21 c22         c21 c22
    
    The Φ represents an out-of-plane magnetic field. The phase is added along the == bond that form closed plaquettes in each unit cell.
    
    :param int N_c: number of unit cells to include in the cell
    :param dict system_params: parameters
    :param str orbitals: indicates whether we have s or p orbitals. input is either 's' or 'p'. If input is 'p', only A,B,C sites host p orbitals, 
    :param bool semi_infinite: whether to make a semi-infinite chain or not
    
    '''
    
    #make builder and populate with onsite and hoppings
    if semi_infinite:
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,-1]))
    else:
        syst = kwant.Builder()
    
        
    field_phase = np.exp(1j*system_params['phi_d'])
    
    if orbitals == 's':
    
        lat = kwant.lattice.Polyatomic(prim_vecs = [[1,-1],[1,1]], basis = [[0,0], 
                                                                            [1,0], 
                                                                            [0,-1], 
                                                                            [0.5,0], 
                                                                            [0,-0.5], 
                                                                            [0,0.5], 
                                                                            [-0.5,0],
                                                                            [1,0.5],
                                                                            [1.5,0],
                                                                            [-0.5,-1],
                                                                            [0,-1.5]
                                                                           ], 
                                                                           norbs = 1)
        
        a_lat, b_lat, c_lat, ab_lat, ac_lat, ba_lat, ca_lat, b21_lat,b22_lat, c21_lat,c22_lat = lat.sublattices
        
        
        
        for i in range(N_c):
            
            if system_params['broken_inversion'] and i >= system_params['inversion_breaking_point']:
                b_a_hop = system_params['j2'] + system_params['b']
                c_a_hop = system_params['j2']
                bb_hop = system_params['j2']
                cc_hop = system_params['j2']
            else:
                b_a_hop = system_params['j2'] 
                c_a_hop = system_params['j2']
                bb_hop = system_params['j2']
                cc_hop = system_params['j2']
        
            #staggered point
            sp = i

            syst[a_lat(sp,0)] = 0
            syst[b_lat(sp,0)] = 0
            syst[c_lat(sp,0)] = 0
            syst[ab_lat(sp,0)] = system_params['V1']
            syst[ac_lat(sp,0)] = system_params['V1']
            syst[ba_lat(sp,0)] = system_params['V1']
            syst[ca_lat(sp,0)] = system_params['V1']
            syst[b21_lat(sp,0)] = system_params['V1']
            syst[c21_lat(sp,0)] = system_params['V1']
            syst[b22_lat(sp,0)] = system_params['V1']
            syst[c22_lat(sp,0)] = system_params['V1']
            
            
            syst[ba_lat(sp+1,0)] = 0
            syst[ca_lat(sp+1,0)] = 0
            
            if i == N_c-1: #last one
                field_phase_ = 1
            else:
                field_phase_ = field_phase
            syst[b_lat(sp,0), ba_lat(sp+1,0), ] = b_a_hop*field_phase
            syst[c_lat(sp,0), ca_lat(sp+1,0), ] = c_a_hop
                
                
            if i == 0 : #first one
                field_phase__ = 1
            else:
                field_phase__ = field_phase
            syst[ba_lat(sp,0), a_lat(sp,0)] = b_a_hop*field_phase
            syst[a_lat(sp,0), ab_lat(sp,0)] = b_a_hop
            syst[ab_lat(sp,0), b_lat(sp,0)] = b_a_hop
            syst[b21_lat(sp,0), b_lat(sp,0)] = bb_hop
            syst[b22_lat(sp,0), b_lat(sp,0)] = bb_hop
            
            syst[ca_lat(sp,0), a_lat(sp,0)] = c_a_hop
            syst[a_lat(sp,0), ac_lat(sp,0)] = c_a_hop
            syst[ac_lat(sp,0), c_lat(sp,0)] = c_a_hop
            syst[c21_lat(sp,0), c_lat(sp,0)] = cc_hop
            syst[c22_lat(sp,0), c_lat(sp,0)] = cc_hop
            

        
    elif orbitals == 'p':
        
        complex_phase = np.exp(1j*system_params['phi']) #physical choice: phi = pi for hopping between p orbitals. for s-p: ?
        
        
        lat = kwant.lattice.Polyatomic(prim_vecs = [[1,-1],[1,1]], basis = [[0,0],[0,0], 
                                                                            [1,0],[1,0],
                                                                            [0,-1],[0,-1],
                                                                            [0.5,0],[0.5,0],
                                                                            [0,-0.5],[0,-0.5],
                                                                            [0,0.5],[0,0.5],
                                                                            [-0.5,0],[-0.5,0],
                                                                            [1,0.5],[1,0.5],
                                                                            [1.5,0],[1.5,0],
                                                                            [-0.5,-1],[-0.5,-1],
                                                                            [0,-1.5],[0,-1.5]
                                                                           ], 
                                                                           norbs = 1)
        a_lat_pos,a_lat_neg, b_lat_pos,b_lat_neg, c_lat_pos,c_lat_neg, ab_lat_pos,ab_lat_neg, ac_lat_pos,ac_lat_neg, ba_lat_pos,ba_lat_neg, ca_lat_pos,ca_lat_neg, b21_lat_pos,b21_lat_neg,b22_lat_pos,b22_lat_neg, c21_lat_pos,c21_lat_neg,c22_lat_pos,c22_lat_neg, = lat.sublattices
        
        
    
        for i in range(N_c):
            
            if system_params['broken_inversion'] and i >= system_params['inversion_breaking_point']:
                b_a_hop = system_params['j2'] + system_params['b']
                c_a_hop = system_params['j2']
                bb_hop = system_params['j2']
                cc_hop = system_params['j2']
            else:
                b_a_hop = system_params['j2'] 
                c_a_hop = system_params['j2']
                bb_hop = system_params['j2']
                cc_hop = system_params['j2']
        
            #staggered point
            sp = i

            syst[a_lat_pos(sp,0)] = 0
            syst[b_lat_pos(sp,0)] = 0
            syst[c_lat_pos(sp,0)] = 0
            syst[ab_lat_pos(sp,0)] = system_params['V1']
            syst[ac_lat_pos(sp,0)] = system_params['V1']
            syst[ba_lat_pos(sp,0)] = system_params['V1']
            syst[ca_lat_pos(sp,0)] = system_params['V1']
            syst[b21_lat_pos(sp,0)] = system_params['V1']
            syst[c21_lat_pos(sp,0)] = system_params['V1']
            syst[b22_lat_pos(sp,0)] = system_params['V1']
            syst[c22_lat_pos(sp,0)] = system_params['V1']
            
            syst[a_lat_neg(sp,0)] = 0
            syst[b_lat_neg(sp,0)] = 0
            syst[c_lat_neg(sp,0)] = 0
            syst[ab_lat_neg(sp,0)] = system_params['V1']
            syst[ac_lat_neg(sp,0)] = system_params['V1']
            syst[ba_lat_neg(sp,0)] = system_params['V1']
            syst[ca_lat_neg(sp,0)] = system_params['V1']
            syst[b21_lat_neg(sp,0)] = system_params['V1']
            syst[c21_lat_neg(sp,0)] = system_params['V1']
            syst[b22_lat_neg(sp,0)] = system_params['V1']
            syst[c22_lat_neg(sp,0)] = system_params['V1']
            
            syst[ba_lat_pos(sp+1,0)] = 0
            syst[ca_lat_pos(sp+1,0)] = 0
            syst[ba_lat_neg(sp+1,0)] = 0
            syst[ca_lat_neg(sp+1,0)] = 0
            
            
            syst[b_lat_pos(sp,0), ba_lat_pos(sp+1,0), ] = b_a_hop
            syst[c_lat_pos(sp,0), ca_lat_pos(sp+1,0), ] = c_a_hop
            
            syst[b_lat_neg(sp,0), ba_lat_neg(sp+1,0), ] = b_a_hop
            syst[c_lat_neg(sp,0), ca_lat_neg(sp+1,0), ] = c_a_hop
            
            syst[b_lat_pos(sp,0), ba_lat_neg(sp+1,0), ] = b_a_hop*complex_phase
            syst[c_lat_pos(sp,0), ca_lat_neg(sp+1,0), ] = c_a_hop
            
            syst[b_lat_neg(sp,0), ba_lat_pos(sp+1,0), ] = b_a_hop*complex_phase
            syst[c_lat_neg(sp,0), ca_lat_pos(sp+1,0), ] = c_a_hop
            

            syst[ba_lat_pos(sp,0), a_lat_pos(sp,0)] = b_a_hop
            syst[a_lat_pos(sp,0), ab_lat_pos(sp,0)] = b_a_hop
            syst[ab_lat_pos(sp,0), b_lat_pos(sp,0)] = b_a_hop
            syst[b21_lat_pos(sp,0), b_lat_pos(sp,0)] = bb_hop
            syst[b22_lat_pos(sp,0), b_lat_pos(sp,0)] = bb_hop

            syst[ba_lat_neg(sp,0), a_lat_neg(sp,0)] = b_a_hop
            syst[a_lat_neg(sp,0), ab_lat_neg(sp,0)] = b_a_hop
            syst[ab_lat_neg(sp,0), b_lat_neg(sp,0)] = b_a_hop
            syst[b21_lat_neg(sp,0), b_lat_neg(sp,0)] = bb_hop
            syst[b22_lat_neg(sp,0), b_lat_neg(sp,0)] = bb_hop
            
            syst[ba_lat_pos(sp,0), a_lat_neg(sp,0)] = b_a_hop*complex_phase
            syst[a_lat_pos(sp,0), ab_lat_neg(sp,0)] = b_a_hop
            syst[ab_lat_pos(sp,0), b_lat_neg(sp,0)] = b_a_hop
            syst[b21_lat_pos(sp,0), b_lat_neg(sp,0)] = bb_hop#*complex_phase
            syst[b22_lat_pos(sp,0), b_lat_neg(sp,0)] = bb_hop
            
            syst[ba_lat_neg(sp,0), a_lat_pos(sp,0)] = b_a_hop*complex_phase
            syst[a_lat_neg(sp,0), ab_lat_pos(sp,0)] = b_a_hop
            syst[ab_lat_neg(sp,0), b_lat_pos(sp,0)] = b_a_hop
            syst[b21_lat_neg(sp,0), b_lat_pos(sp,0)] = bb_hop#*complex_phase
            syst[b22_lat_neg(sp,0), b_lat_pos(sp,0)] = bb_hop
            
            
            syst[ca_lat_pos(sp,0), a_lat_pos(sp,0)] = c_a_hop
            syst[a_lat_pos(sp,0), ac_lat_pos(sp,0)] = c_a_hop
            syst[ac_lat_pos(sp,0), c_lat_pos(sp,0)] = c_a_hop
            syst[c21_lat_pos(sp,0), c_lat_pos(sp,0)] = cc_hop
            syst[c22_lat_pos(sp,0), c_lat_pos(sp,0)] = cc_hop
            
            syst[ca_lat_neg(sp,0), a_lat_neg(sp,0)] = c_a_hop
            syst[a_lat_neg(sp,0), ac_lat_neg(sp,0)] = c_a_hop
            syst[ac_lat_neg(sp,0), c_lat_neg(sp,0)] = c_a_hop
            syst[c21_lat_neg(sp,0), c_lat_neg(sp,0)] = cc_hop
            syst[c22_lat_neg(sp,0), c_lat_neg(sp,0)] = cc_hop
            
            syst[ca_lat_pos(sp,0), a_lat_neg(sp,0)] = c_a_hop
            syst[a_lat_pos(sp,0), ac_lat_neg(sp,0)] = c_a_hop*complex_phase
            syst[ac_lat_pos(sp,0), c_lat_neg(sp,0)] = c_a_hop*complex_phase
            syst[c_lat_pos(sp,0), c21_lat_neg(sp,0)] = cc_hop
            syst[c_lat_pos(sp,0), c22_lat_neg(sp,0)] = cc_hop#*complex_phase
            
            syst[ca_lat_neg(sp,0), a_lat_pos(sp,0)] = c_a_hop
            syst[a_lat_neg(sp,0), ac_lat_pos(sp,0)] = c_a_hop*complex_phase
            syst[ac_lat_neg(sp,0), c_lat_pos(sp,0)] = c_a_hop*complex_phase
            syst[c_lat_pos(sp,0), c21_lat_neg(sp,0)] = cc_hop
            syst[c_lat_pos(sp,0), c22_lat_neg(sp,0)] = cc_hop#*complex_phase
            
    
    return syst
    



def diamond_chain_s_p(N_c, system_params, semi_infinite=False, s_or_p = 's'):
    '''
    Create a diamond chain of trimer unit cells. We just have particle-in-a-box states. We have secondary sites, that either host s or p sites, depending on the hopping coupling. the s states have to have two orbitals, in order to match the two orbitals on the p sites.
          
          _______      _______
         | A     | _o_ | B     |
         |       |     |       |
          -------       ------- 
              o     Φ    ||
          _______       
         | C     | _o_  ...
         |       |  
          -------    
                          
                         
    The Φ represents an out-of-plane magnetic field. The phase is added along the == bond in each unit cell 
    
    :param int N_c: number of unit cells to include in the cell
    :param dict system_params: parameters
    :param bool semi_infinite: whether to make a semi-infinite chain or not
    :param str s_or_p: Whether the secondary sites host s states or p states.
    
    :rtype kwant.system.FiniteSystem:
    '''
    
    # make lattices and sublattices
    lat = kwant.lattice.Polyatomic(prim_vecs = [[1,-1],[1,1]], basis = [[0,0],[0,0], 
                                                                        [1,0],[1,0], 
                                                                        [0,-1],[0,-1], 
                                                                        [0.5,0],[0.5,0],
                                                                        [0,-0.5],[0,-0.5], 
                                                                        [1,-0.5],[1,-0.5], 
                                                                        [0.5,-1],[0.5,-1]], norbs = 1)
    
    a_lat_pos, a_lat_neg, b_lat_pos, b_lat_neg, c_lat_pos, c_lat_neg, ab_lat_pos, ab_lat_neg, ac_lat_pos, ac_lat_neg, ba_lat_pos, ba_lat_neg, ca_lat_pos, ca_lat_neg = lat.sublattices
    
    #make builder and populate with onsite and hoppings
    if semi_infinite:
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,-1]))
    else:
        syst = kwant.Builder()
    
    if system_params['broken_inversion']:
        b_a_hop = system_params['j2'] + system_params['b']
        c_a_hop = system_params['j2'] + system_params['a']
    else:
        b_a_hop = system_params['j2'] 
        c_a_hop = system_params['j2']
    
    if s_or_p == 'p':
        cross_hopping = np.exp(1j*2*np.pi) 
        straight_hopping = 1
    else:
        cross_hopping = np.exp(1j*2*system_params['phi_s']) 
        straight_hopping = cross_hopping
    
    field_phase = np.exp(1j*2*system_params['phi_b']/2/2)
    
    for i in range(N_c):
        
        #staggered point
        sp = i
        
        syst[a_lat_pos(sp,0)] = 0
        syst[a_lat_neg(sp,0)] = 0
        syst[b_lat_pos(sp,0)] = 0
        syst[b_lat_neg(sp,0)] = 0
        syst[c_lat_pos(sp,0)] = 0
        syst[c_lat_neg(sp,0)] = 0
        
        syst[ab_lat_pos(sp,0)] = system_params['V1']
        syst[ac_lat_pos(sp,0)] = system_params['V1']
        syst[ab_lat_neg(sp,0)] = system_params['V1']
        syst[ac_lat_neg(sp,0)] = system_params['V1']
        
        
        if i < N_c - 1:
            
            syst[a_lat_pos(sp+1,0)] = 0
            syst[a_lat_neg(sp+1,0)] = 0
                
            syst[ba_lat_pos(sp,0)] = system_params['V1']
            syst[ca_lat_pos(sp,0)] = system_params['V1']
            syst[ba_lat_neg(sp,0)] = system_params['V1']
            syst[ca_lat_neg(sp,0)] = system_params['V1']

            syst[a_lat_pos(sp+1,0), ba_lat_pos(sp,0)] = b_a_hop*field_phase
            syst[a_lat_pos(sp+1,0), ca_lat_pos(sp,0)] = c_a_hop*straight_hopping
            syst[a_lat_neg(sp+1,0), ba_lat_neg(sp,0)] = b_a_hop*field_phase
            syst[a_lat_neg(sp+1,0), ca_lat_neg(sp,0)] = c_a_hop*straight_hopping
            
            syst[a_lat_pos(sp+1,0), ba_lat_neg(sp,0)] = b_a_hop*field_phase
            syst[a_lat_pos(sp+1,0), ca_lat_neg(sp,0)] = c_a_hop*cross_hopping
            syst[a_lat_neg(sp+1,0), ba_lat_pos(sp,0)] = b_a_hop*field_phase
            syst[a_lat_neg(sp+1,0), ca_lat_pos(sp,0)] = c_a_hop*cross_hopping

            syst[ba_lat_pos(sp,0), b_lat_pos(sp,0)] = b_a_hop*field_phase
            syst[ca_lat_pos(sp,0), c_lat_pos(sp,0)] = c_a_hop*straight_hopping
            syst[ba_lat_neg(sp,0), b_lat_neg(sp,0)] = b_a_hop*field_phase
            syst[ca_lat_neg(sp,0), c_lat_neg(sp,0)] = c_a_hop*straight_hopping
            
            syst[ba_lat_pos(sp,0), b_lat_neg(sp,0)] = b_a_hop*field_phase
            syst[ca_lat_pos(sp,0), c_lat_neg(sp,0)] = c_a_hop*cross_hopping
            syst[ba_lat_neg(sp,0), b_lat_pos(sp,0)] = b_a_hop*field_phase
            syst[ca_lat_neg(sp,0), c_lat_pos(sp,0)] = c_a_hop*cross_hopping
                
          
        
        syst[a_lat_pos(sp,0), ac_lat_pos(sp,0)] = c_a_hop
        syst[c_lat_pos(sp,0), ac_lat_pos(sp,0)] = c_a_hop
        syst[a_lat_neg(sp,0), ac_lat_neg(sp,0)] = c_a_hop
        syst[c_lat_neg(sp,0), ac_lat_neg(sp,0)] = c_a_hop
        
        syst[a_lat_pos(sp,0), ac_lat_neg(sp,0)] = c_a_hop
        syst[c_lat_pos(sp,0), ac_lat_neg(sp,0)] = c_a_hop
        syst[a_lat_neg(sp,0), ac_lat_pos(sp,0)] = c_a_hop
        syst[c_lat_neg(sp,0), ac_lat_pos(sp,0)] = c_a_hop


        syst[a_lat_pos(sp,0), ab_lat_pos(sp,0)] = b_a_hop*straight_hopping
        syst[b_lat_pos(sp,0), ab_lat_pos(sp,0)] = b_a_hop*straight_hopping
        syst[a_lat_neg(sp,0), ab_lat_neg(sp,0)] = b_a_hop*straight_hopping
        syst[b_lat_neg(sp,0), ab_lat_neg(sp,0)] = b_a_hop*straight_hopping
        
        syst[a_lat_pos(sp,0), ab_lat_neg(sp,0)] = b_a_hop*cross_hopping
        syst[b_lat_pos(sp,0), ab_lat_neg(sp,0)] = b_a_hop*cross_hopping
        syst[a_lat_neg(sp,0), ab_lat_pos(sp,0)] = b_a_hop*cross_hopping
        syst[b_lat_neg(sp,0), ab_lat_pos(sp,0)] = b_a_hop*cross_hopping
        
    
    return syst



def diamond_chain_system(N_c, system_params, semi_infinite = False, leads = False):
    '''
    Create a diamond chain of trimer unit cells. Each atom of the trimer has two orbital angular momentum states, + and -
          _______
    ...  | C_i_+ |
         | C_i_- |         ...
        / ------- \ ______ /
    ...           | A_i_+ | ...
             Φ    | A_i_- |
        \ ______ //------- \
    ...  | B_i_+ |         ...
         | B_i_- |
          ------- 
    
    The Φ represents an out-of-plane magnetic field. The phase is added along the // bond in each unit cell 
    
    :param int N_c: number of unit cells to include in the cell
    :param dict system_params: parameters
    :param bool semi_infinite: whether to make a semi-infinite chain or not
    :param bool leads: whether to include leads 
    
    :rtype kwant.system.FiniteSystem:
    '''
    
    # make lattices and sublattices
    lat = kwant.lattice.Polyatomic(prim_vecs = [[1,0],[0,1]], basis = [[1,0],[1,0], [0,-1],[0,-1], [0,1],[0,1]], norbs = 1)
    a_lat_pos, a_lat_neg, b_lat_pos, b_lat_neg, c_lat_pos, c_lat_neg = lat.sublattices
    
    #make builder and populate with onsite and hoppings
    if semi_infinite == True:
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,0]))
    else:
        syst = kwant.Builder()
    
    
    for i in range(N_c):
        
        #staggered point
        sp = i
        
        syst[a_lat_pos(sp,0)] = system_params['mu_a_pos']
        syst[a_lat_neg(sp,0)] = system_params['mu_a_neg']
        syst[b_lat_pos(sp,0)] = system_params['mu_b_pos']
        syst[b_lat_neg(sp,0)] = system_params['mu_b_neg']
        syst[c_lat_pos(sp,0)] = system_params['mu_c_pos']
        syst[c_lat_neg(sp,0)] = system_params['mu_c_neg']
        
        if i < N_c - 1:
        
            
            syst[c_lat_pos(sp+1,0)] = system_params['mu_c_pos']
            syst[b_lat_pos(sp+1,0)] = system_params['mu_b_pos']
            syst[c_lat_neg(sp+1,0)] = system_params['mu_c_neg']
            syst[b_lat_neg(sp+1,0)] = system_params['mu_b_neg']

            # + <--> + 
            syst[a_lat_pos(sp,0), c_lat_pos(sp+1,0)] = system_params['h_plus_plus'] #j2
            syst[a_lat_pos(sp,0), b_lat_pos(sp+1,0)] = system_params['h_plus_plus'] #j2

            # - <--> -
            syst[a_lat_neg(sp,0), c_lat_neg(sp+1,0)] = system_params['h_minus_minus'] #j2
            syst[a_lat_neg(sp,0), b_lat_neg(sp+1,0)] = system_params['h_minus_minus'] #j2

            # + <--> - 
            syst[a_lat_pos(sp,0), b_lat_neg(sp+1,0)] = system_params['h_bar_plus_minus']*np.exp(1j*2*system_params['phi']) #phase e^i phi #j3
            syst[a_lat_neg(sp,0), b_lat_pos(sp+1,0)] = system_params['h_bar_minus_plus']*np.exp(1j*2*system_params['phi']) #phase e^i phi #j3

            # + <--> - hopping with phase
            syst[a_lat_pos(sp,0), c_lat_neg(sp+1,0)] = system_params['h_plus_minus']
            syst[a_lat_neg(sp,0), c_lat_pos(sp+1,0)] = system_params['h_minus_plus']
            
        # + <--> + 
        syst[a_lat_pos(sp,0), c_lat_pos(sp,0)] = system_params['h_plus_plus']
        if i == 0:
            added_phase = 1
        else:
            added_phase = np.exp(1j*2*system_params['phi_d'])
        syst[a_lat_pos(sp,0), b_lat_pos(sp,0)] = system_params['h_plus_plus']*added_phase #phase e^i phi_d due to mag field
        # - <--> -
        syst[a_lat_neg(sp,0), c_lat_neg(sp,0)] = system_params['h_minus_minus']
        syst[a_lat_neg(sp,0), b_lat_neg(sp,0)] = system_params['h_minus_minus']*added_phase #phase e^i phi_d due to mag field
        # + <--> - 
        syst[a_lat_pos(sp,0), c_lat_neg(sp,0)] = system_params['h_bar_plus_minus']*np.exp(1j*2*system_params['phi']) #phase e^i phi
        syst[a_lat_neg(sp,0), c_lat_pos(sp,0)] = system_params['h_bar_minus_plus']*np.exp(1j*2*system_params['phi']) #phase e^i phi
        # + <--> - hopping with phase
        syst[a_lat_pos(sp,0), b_lat_neg(sp,0)] = system_params['h_plus_minus']*added_phase #phase e^i phi_d due to mag field 
        syst[a_lat_neg(sp,0), b_lat_pos(sp,0)] = system_params['h_minus_plus']*added_phase #phase e^i phi_d due to mag field #phase e^i phi
        
    if leads:
        
        lead_syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,0]))
        
        
        lead_syst[a_lat_pos(0,0)] = 0
        lead_syst[a_lat_neg(0,0)] = 0
        lead_syst[b_lat_pos(0,0)] = 0
        lead_syst[b_lat_neg(0,0)] = 0
        lead_syst[c_lat_pos(0,0)] = 0
        lead_syst[c_lat_neg(0,0)] = 0
        
        lead_syst[lat.neighbors(n=1)] = 1
        
        syst.attach_lead(lead_syst)
        syst.attach_lead(lead_syst.reversed())
    
    return syst

def tilted_diamond_chain_system(system_params, semi_infinite = False, leads = False, closed_chain = False, broken_inversion = False):
    '''
    Create a diamond chain of trimer unit cells. Each atom of the trimer has two orbital angular momentum states, + and -
          
                      _______
              ...  ==| B_i_+ |
                     | B_i_- |
                      ------- 
              |     Φ    |
           _______    _______
          | C_i_+ |__| A_i_+ |__ ...
          | C_i_- |  | A_i_- |
           -------    -------
                          |
                         ...
                         
    The Φ represents an out-of-plane magnetic field. The phase is added along the == bond in each unit cell 
    
    :param int l: the orbital angular momentum number. p bands have l=1, and d have l=2.
    :param int N_c: number of unit cells to include in the cell
    :param dict system_params: parameters
    :param bool semi_infinite: whether to make a semi-infinite chain or not
    :param bool leads: whether to include leads 
    :param bool closed_chain: whether to close the chain by adding one additional A site on the open trimer at one end of the chain.
    :param bool broken_inversion: whether to break inversion symmetry across the center axis of the chain by making C_i - A_i hoppings different from B_i-A_i hoppings.
    
    :rtype kwant.system.FiniteSystem:
    '''
    # make lattices and sublattices
    lat = kwant.lattice.Polyatomic(prim_vecs = [[1,-1],[1,1]], basis = [[0,0],[0,0], [0,1],[0,1], [-1,0],[-1,0]], norbs = 1)
    a_lat_pos, a_lat_neg, b_lat_pos, b_lat_neg, c_lat_pos, c_lat_neg = lat.sublattices
    
    #make builder and populate with onsite and hoppings
    if semi_infinite:
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,-1]))
    else:
        syst = kwant.Builder()
    
    if broken_inversion:
        b_a_hop_2 = system_params['j2'] + system_params['b']
        c_a_hop_2 = system_params['j2']
        b_a_hop_3 = system_params['j3'] + system_params['b']
        c_a_hop_3 = system_params['j3']
    else:
        b_a_hop_2 = system_params['j2'] 
        c_a_hop_2 = system_params['j2']
        b_a_hop_3 = system_params['j3'] 
        c_a_hop_3 = system_params['j3']
        
    complex_phase = np.exp(1j*system_params['phi'])
    
    for i in range(system_params['N_c']):
        
        #staggered point
        sp = i
        
        syst[a_lat_pos(sp,0)] = system_params['mu_a_pos']
        syst[a_lat_neg(sp,0)] = system_params['mu_a_neg']
        syst[b_lat_pos(sp,0)] = system_params['mu_b_pos']
        syst[b_lat_neg(sp,0)] = system_params['mu_b_neg']
        syst[c_lat_pos(sp,0)] = system_params['mu_c_pos']
        syst[c_lat_neg(sp,0)] = system_params['mu_c_neg']
        
        
        
        if i == 0:
            
            if closed_chain:
                syst[a_lat_pos(-1,0)] = system_params['mu_a_pos']
                syst[a_lat_neg(-1,0)] = system_params['mu_a_neg']
                
                syst[a_lat_pos(-1,0), c_lat_pos(0,0)] = c_a_hop_2
                syst[a_lat_pos(-1,0), b_lat_pos(0,0)] = b_a_hop_2
                
                syst[a_lat_neg(-1,0), c_lat_neg(0,0)] = c_a_hop_2
                syst[a_lat_neg(-1,0), b_lat_neg(0,0)] = b_a_hop_2
                
                syst[a_lat_pos(-1,0), b_lat_neg(0,0)] = b_a_hop_3*complex_phase
                syst[a_lat_neg(-1,0), b_lat_pos(0,0)] = b_a_hop_3*complex_phase
                
                syst[a_lat_pos(-1,0), c_lat_neg(0,0)] = c_a_hop_3
                syst[a_lat_neg(-1,0), c_lat_pos(0,0)] = c_a_hop_3
            
        
        if i < system_params['N_c'] - 1:
        
            
            syst[c_lat_pos(sp+1,0)] = system_params['mu_c_pos']
            syst[b_lat_pos(sp+1,0)] = system_params['mu_b_pos']
            syst[c_lat_neg(sp+1,0)] = system_params['mu_c_neg']
            syst[b_lat_neg(sp+1,0)] = system_params['mu_b_neg']

            # + <--> + 
            syst[a_lat_pos(sp,0), c_lat_pos(sp+1,0)] = c_a_hop_2
            syst[a_lat_pos(sp,0), b_lat_pos(sp+1,0)] = b_a_hop_2

            # - <--> -
            syst[a_lat_neg(sp,0), c_lat_neg(sp+1,0)] = c_a_hop_2
            syst[a_lat_neg(sp,0), b_lat_neg(sp+1,0)] = b_a_hop_2

            # + <--> - 
            syst[a_lat_pos(sp,0), b_lat_neg(sp+1,0)] = system_params['j3']*complex_phase
            syst[a_lat_neg(sp,0), b_lat_pos(sp+1,0)] = system_params['j3']*complex_phase

            # + <--> - hopping with phase
            syst[a_lat_pos(sp,0), c_lat_neg(sp+1,0)] = system_params['j3']
            syst[a_lat_neg(sp,0), c_lat_pos(sp+1,0)] = system_params['j3']
            
        # + <--> + 
        syst[a_lat_pos(sp,0), c_lat_pos(sp,0)] = c_a_hop_2
        syst[a_lat_pos(sp,0), b_lat_pos(sp,0)] = b_a_hop_2
        # - <--> -
        syst[a_lat_neg(sp,0), c_lat_neg(sp,0)] = c_a_hop_2
        syst[a_lat_neg(sp,0), b_lat_neg(sp,0)] = b_a_hop_2
        # + <--> - 
        syst[a_lat_pos(sp,0), c_lat_neg(sp,0)] = c_a_hop_3*complex_phase
        syst[a_lat_neg(sp,0), c_lat_pos(sp,0)] = c_a_hop_3*complex_phase
        # + <--> - hopping with phase
        syst[a_lat_pos(sp,0), b_lat_neg(sp,0)] = b_a_hop_3
        syst[a_lat_neg(sp,0), b_lat_pos(sp,0)] = b_a_hop_3
        
            
        
    if leads:
        
        lead_syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,0]))
        
        
        lead_syst[a_lat_pos(0,0)] = 0
        lead_syst[a_lat_neg(0,0)] = 0
        lead_syst[b_lat_pos(0,0)] = 0
        lead_syst[b_lat_neg(0,0)] = 0
        lead_syst[c_lat_pos(0,0)] = 0
        lead_syst[c_lat_neg(0,0)] = 0
        
        lead_syst[lat.neighbors(n=1)] = 1
        
        syst.attach_lead(lead_syst)
        syst.attach_lead(lead_syst.reversed())
    
    return syst




def s_tilted_diamond_chain_system(system_params, semi_infinite = False, leads = False, closed_chain = False, broken_inversion=False):
    '''
    Create a diamond chain of trimer unit cells. 
          
                      _______
              ...  __| B_i   |
                     |       |
                      ------- 
              |     Φ    ||
           _______    _______
          | C_i   |__| A_i   |__ ...
          |       |  |       |
           -------    -------
                          |
                         ...
                         
    The Φ represents an out-of-plane magnetic field. The phase is added along the == bond in each unit cell 
    
    :param int N_c: number of unit cells to include in the cell
    :param dict system_params: parameters
    :param bool semi_infinite: whether to make a semi-infinite chain or not
    :param bool leads: whether to include leads 
    :param bool closed_chain: whether to close the chain by adding one additional A site on the open trimer at one end of the chain.
    :param bool broken_inversion: whether to break inversion symmetry by making a-c and a-b hoppings asymmetric
    
    :rtype kwant.system.FiniteSystem:
    '''
    
    # make lattices and sublattices
    lat = kwant.lattice.Polyatomic(prim_vecs = [[1,-1],[1,1]], basis = [[0,0], [0,1], [-1,0]], norbs = 1)
    a_lat, b_lat, c_lat = lat.sublattices
    
    #make builder and populate with onsite and hoppings
    if semi_infinite == True:
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,-1]))
    else:
        syst = kwant.Builder()
    
    if broken_inversion:
        b_a_hop_2 = system_params['j2'] + system_params['b']
        c_a_hop_2 = system_params['j2']
    else:
        b_a_hop_2 = system_params['j2'] 
        c_a_hop_2 = system_params['j2']
    
    for i in range(system_params['N_c']):
        
        #staggered point
        sp = i
        
        syst[a_lat(sp,0)] = system_params['mu_a']
        syst[b_lat(sp,0)] = system_params['mu_b']
        syst[c_lat(sp,0)] = system_params['mu_c']
        
        if i == 0:
            
            added_phase = 1
            
            if closed_chain:
                syst[a_lat(-1,0)] = system_params['mu_a']
                
                syst[a_lat(-1,0), c_lat(0,0)] = c_a_hop_2
                syst[a_lat(-1,0), b_lat(0,0)] = b_a_hop_2
                
            
        else:
            added_phase = np.exp(1j*2*system_params['phi_d'])
            
        
        if i < system_params['N_c'] - 1:
        
            #intercell
            syst[c_lat(sp+1,0)] = system_params['mu_c']
            syst[b_lat(sp+1,0)] = system_params['mu_b']

            syst[a_lat(sp,0), c_lat(sp+1,0)] = c_a_hop_2
            syst[a_lat(sp,0), b_lat(sp+1,0)] = b_a_hop_2

            
        # intracell
        syst[a_lat(sp,0), c_lat(sp,0)] = c_a_hop_2
        syst[a_lat(sp,0), b_lat(sp,0)] = b_a_hop_2*added_phase #phase e^i phi_d due to mag field
        
        
            
        
    if leads:
        
        lead_syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry([1,0]))
        
        
        lead_syst[a_lat(0,0)] = 0
        lead_syst[b_lat(0,0)] = 0
        lead_syst[c_lat(0,0)] = 0
        
        lead_syst[lat.neighbors(n=1)] = 1
        
        syst.attach_lead(lead_syst)
        syst.attach_lead(lead_syst.reversed())
    
    return syst


def ssh_chain(system_params,trivial=False):
    '''
    Build a SSH chain using the s orbital model.
    '''
    
    lat = kwant.lattice.square(1,norbs=1)
    syst = kwant.Builder()
    hop_strong = system_params['j2'] 
    hop_weak = hop_strong/system_params['difference_in_bond_strength']
    
    for i in range(8):
        syst[lat(i,0)] = system_params['mu']
        
    syst[kwant.builder.HoppingKind((1, 0), lat, lat)] = hop_strong
    
    if trivial:
        syst[lat(1,0),lat(2,0)] = hop_weak
        syst[lat(2,0),lat(1,0)] = hop_weak
        
        syst[lat(3,0),lat(4,0)] = hop_weak
        syst[lat(4,0),lat(3,0)] = hop_weak
        
        syst[lat(5,0),lat(6,0)] = hop_weak
        syst[lat(6,0),lat(5,0)] = hop_weak
            
    else:
        for i in range(8,10):
            syst[lat(i,0)] = system_params['mu']
        
        syst[lat(1,0),lat(2,0)] = hop_weak
        syst[lat(2,0),lat(1,0)] = hop_weak
        
        syst[lat(2,0),lat(3,0)] = hop_weak
        syst[lat(3,0),lat(2,0)] = hop_weak
        
        syst[lat(4,0),lat(5,0)] = hop_weak
        syst[lat(5,0),lat(4,0)] = hop_weak
        
        syst[lat(6,0),lat(7,0)] = hop_weak
        syst[lat(7,0),lat(6,0)] = hop_weak
        
        syst[lat(7,0),lat(8,0)] = hop_weak
        syst[lat(8,0),lat(7,0)] = hop_weak
        
        
        
    return syst


def chain_with_overlap(kwant_system_creator_function, params):
    
    kwant_system = kwant_system_creator_function(**params['model_params'])
    
    ham = kwant_system.finalized().hamiltonian_submatrix()

    diagonal_elements = ham.diagonal()
    subtraction_matrix = np.zeros(ham.shape)
    for ind in range(ham.shape[0]): #square matrix so we're fine
        one_mat = np.zeros(ham.shape)
        one_mat[ind][ind] = 1
        subtraction_matrix += np.real(np.kron( one_mat , diagonal_elements[ind] ))
    ham_only_off_diagonals = ham - subtraction_matrix

    mat_es = np.zeros(ham.shape)
    mat_es[ np.where(ham_only_off_diagonals != 0) ] = params['orbital_overlap']
    
    mat_es += np.eye(ham.shape[0]) #add the diagonal back in w/ factor 1
    
    #here's where we solve the generalized eigenvalue problem 
    ev, es = scipy.linalg.eig(ham, b=mat_es)
    
    return ev, es
