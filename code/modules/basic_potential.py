import kwant
import scipy.sparse.linalg as sla
import numpy as np
from matplotlib import pyplot as plt
import itertools as it

import matplotlib.gridspec as gridspec

# +
#physical constants
hbar = 1
m = 1
a = 6.75 #0.36 nm (lattice constant) in a.u.

#for kwant
lattice_spacing = 1
t = hbar**2/(2*m*lattice_spacing**2) #sets the hopping parameter (the off-diagonal elements of the hamiltonian)


def quartic_chain_1_n(site, n, Nc, a, Vo, V1, start_pos, closed_chain, dim=1, with_embellishments=True, square_potential = True):
    '''
    create a chain like the quartic TI in arxiv:2102.12635
    
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param int Nc: the number of the unit cell; affects the start position. python counting (starts at 0).
    :param array start_pos: the start position of the top left corner of the chain
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    '''
    
    #connecting patch sizes
    big_n = n-2
    small_n = n-4
    
    for Uc in range(Nc):
        cell = quartic_trimer_1_n(site,a,Vo,V1,n,[start_pos[0]+Uc*a*(n+2*small_n+big_n), start_pos[1]+Uc*a*(n+2*small_n+big_n)],in_chain_bulk=False, with_embellishments=with_embellishments, square_potential = square_potential)
        if cell != Vo:
            return cell
            
    return Vo

def quartic_chain_2_n(site, n, Nc, a, Vo, V1, start_pos, closed_chain, dim=1, with_embellishments=True, square_potential = True):
    '''
    create a chain like the quartic TI in arxiv:2102.12635, all sites the same size (except for actual connecting patches)
    
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param int Nc: the number of the unit cell; affects the start position. python counting (starts at 0).
    :param array start_pos: the start position of the top left corner of the chain
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    '''
    
    #connecting patch sizes
    big_n = n
    small_n = n-4
    
    for Uc in range(Nc):
        cell = quartic_trimer_2_n(site,a,Vo,V1,n,[start_pos[0]+Uc*a*(n+2*small_n+big_n), start_pos[1]+Uc*a*(n+2*small_n+big_n)],in_chain_bulk=False, with_embellishments=with_embellishments, square_potential = square_potential)
        if cell != Vo:
            return cell
            
    return Vo

def homogeneous_chain_n(site, n, Nc, a, Vo, V1, start_pos, closed_chain, start_inversion_breaking_pos, break_inversion=False,  dim=1, square_potential = True):
    '''
    
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param int Nc: the number of the unit cell; affects the start position. python counting (starts at 0).
    :param array start_pos: the start position of the top left corner of the chain
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    '''
    
    
    #connecting patch size
    n_ = np.max([n-1-n%2,1])
    
    for Uc in range(Nc):
        break_inversion_ = [False, break_inversion][Uc >= start_inversion_breaking_pos]
        if Uc != 0:
            cell = homogeneous_trimer_n(site,a,Vo,V1,n,[start_pos[0]+Uc*a*(n+n_), start_pos[1]+Uc*a*(n+n_)],in_chain_bulk=False, break_inversion=break_inversion_,dim=dim, square_potential = square_potential)
            if cell != Vo:
                return cell
        else:
            cell = homogeneous_trimer_n(site,a,Vo,V1,n,start_pos, break_inversion=break_inversion_,dim=dim, in_chain_bulk=True, square_potential = square_potential)
            if cell != Vo:
                return cell
            
    return Vo


def unit_cell_homogeneous_chain_n(site, n, Nc, a, Vo, V1, start_pos, closed_chain, start_inversion_breaking_pos, break_inversion=False,  dim=1, square_potential = True):
    '''
    
         o      o
        ___    ___
      o|   |oo|   |o
        - -    - -  
         o      o
         o   
        ___        etc.
      o|   |o    
        - -
         o
    
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param int Nc: the number of the unit cell; affects the start position. python counting (starts at 0).
    :param array start_pos: the start position of the top left corner of the chain
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    '''
    
    
    #connecting patch size
    n_ = n//2
    
    for Uc in range(Nc):
        break_inversion_ = [False, break_inversion][Uc >= start_inversion_breaking_pos]
        if Uc != 0:
            cell = homogeneous_unit_cell_trimer_n(site,a,Vo,V1,n,[start_pos[0]+Uc*a*(n+2*n_), start_pos[1]+Uc*a*(n+2*n_)],in_chain_bulk=False, break_inversion=break_inversion_,dim=dim, square_potential = square_potential)
            if cell != Vo:
                return cell
        else:
            cell = homogeneous_unit_cell_trimer_n(site,a,Vo,V1,n,start_pos, break_inversion=break_inversion_,dim=dim, in_chain_bulk=True, square_potential = square_potential)
            if cell != Vo:
                return cell
            
    return Vo
    


def chain_n(site, n, Nc, a, Vo, V1,start_pos, closed_chain, break_inversion = False,dim=1, square_potential = True):
    '''
    Create a chain of Nc unit cells composed of trimers of size n.
    
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param int Nc_: the number of the unit cell; affects the start position. python counting (starts at 0).
    :param array start_pos: the start position of the top left corner of the chain
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    '''
    #connecting patch size
    n_ = np.max([n-1-n%2,1])
    
    for Uc in range(Nc):
        if Uc != 0:
            cell = trimer_n(site,a,Vo,V1,n,[start_pos[0]+Uc*a*(n+n_), start_pos[1]+Uc*a*(n+n_)],in_chain_bulk=True, break_inversion=break_inversion,dim=dim, square_potential = square_potential)
            if cell != Vo:
                return cell
        else:
            cell = trimer_n(site,a,Vo,V1,n,start_pos, break_inversion=break_inversion,dim=dim, square_potential = square_potential)
            if cell != Vo:
                return cell
            
#     if closed_chain:
#         extra_patch = patch_n(site,a, Vo, n, [start_pos[0]+Nc*a*(n+n_), start_pos[1]+Nc*a*(n+n_)])
#         if np.allclose(extra_patch , np.eye(2)*0):
#             return 0
#         extra_connector_1_2 = patch_n(site,a, Vo, n_, [start_pos[0]+(Nc*(n+n_)+1)*a, start_pos[1]+(Nc*(n+n_)-n_)*a])
#         if np.allclose(extra_connector_1_2 , np.eye(2)*0):
#             return 0
#         extra_connector_1_3 = patch_n(site,a, Vo, n_, [start_pos[0]+(Nc*(n+n_)-n_)*a, start_pos[1]+(Nc*(n+n_)+1)*a])
#         if np.allclose(extra_connector_1_3 , np.eye(2)*0):
#             return np.eye(2)*0
            
    return Vo


def trimer_n(site,a,Vo,V1,n,start_pos, in_chain_bulk=False, break_inversion = False, dim=1, square_potential = True):
    '''
    Create a trimer potential profile of nxn patches. If n is odd, connecting patches are (n-2)x(n-2) and centered,
    if n is even, connecting patches are (n-1)x(n-1) and snap to the outer edges of the trimer.
    
     __   __
    |1 |=|2 |
     --   --
     |_
    |3 |
     --
    
    :param kwant.builder.site site: a given site 
    :param float a: the effective site size
    :param float Vo: the potential surrounding the patches
    :param float V1: the potential on the connecting patches
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param array start_pos: the start position of the top left corner of the first patch
    :param bool in_chain_bulk: if the trimer is being called as part of a chain, we add connecting patches around the first patch.
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    
    '''
    assert n >= 2
    
    #connecting patch size
    n_ = np.max([n-1-n%2,1])
    
    if break_inversion == False:
        n__ = False
        rectangle_12 = False
        rectangle_13 = False
    elif break_inversion:
        n__ = np.max([n_*a//2,1])
        if break_inversion == 'center_axis':
            rectangle_12 = n__
            rectangle_13 = False
        elif break_inversion == 'unit_cell':
            rectangle_12 = False
            rectangle_13 = n__
        else:
            raise ValueError('specify how the inversion symmetry will be broken: center_axis or unit_cell.')
    
    
    
    patch_1 = patch_n(site,a, Vo, V1, n, start_pos,dim=dim, main=True, square_potential = square_potential)
    if patch_1[0] == 0:
        return patch_1[1]
    
    patch_2 = patch_n(site,a, Vo, V1, n, [start_pos[0]+(n+n_)*a, start_pos[1]],dim=dim, main=True, square_potential = square_potential)
    if patch_2[0] == 0:
        return patch_2[1]
    
    patch_3 = patch_n(site,a, Vo, V1, n, [start_pos[0], start_pos[1]+(n+n_)*a],dim=dim, main=True, square_potential = square_potential)
    if patch_3[0] == 0:
        return patch_3[1]
    
    if n%2 == 0:
        patch_1_2 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+n*a, start_pos[1]],rectangle=rectangle_12,dim=dim, square_potential = square_potential)
        if patch_1_2[0] == 0:
            return patch_1_2[1]
        patch_1_3 = patch_n(site,a, Vo, V1, n_, [start_pos[0], start_pos[1]+n*a],rectangle=rectangle_13,rectangle_y=True,dim=dim, square_potential = square_potential)
        if patch_1_3[0] == 0:
            return patch_1_3[1]
    else:
        patch_1_2 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+n*a, start_pos[1]+1*a],rectangle=rectangle_12,dim=dim, square_potential = square_potential)
        if patch_1_2[0] == 0:
            return patch_1_2[1]
        patch_1_3 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+1*a, start_pos[1]+n*a],rectangle=rectangle_13,rectangle_y=True,dim=dim, square_potential = square_potential)
        if patch_1_3[0] == 0:
            return patch_1_3[1]
    
    if in_chain_bulk:
        if n%2 == 0:
            extra_connector_1_2 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+1*a, start_pos[1]-n_*a],rectangle=n__,rectangle_y=True,dim=dim, square_potential = square_potential)
            if extra_connector_1_2[0] == 0:
                return extra_connector_1_2[1]
            extra_connector_1_3 = patch_n(site,a, Vo, V1, n_, [start_pos[0]-n_*a, start_pos[1]+1*a],dim=dim, square_potential = square_potential)
            if extra_connector_1_3[0] == 0:
                return extra_connector_1_3[1]
        else:
            extra_connector_1_2 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+1*a, start_pos[1]-n_*a],rectangle=n__,rectangle_y=True,dim=dim, square_potential = square_potential)
            if extra_connector_1_2[0] == 0:
                return extra_connector_1_2[1]
            extra_connector_1_3 = patch_n(site,a, Vo, V1, n_, [start_pos[0]-n_*a, start_pos[1]+1*a],dim=dim, square_potential = square_potential)
            if extra_connector_1_3[0] == 0:
                return extra_connector_1_3[1]
    return Vo


def homogeneous_trimer_n(site,a,Vo,V1,n,start_pos, in_chain_bulk=False, break_inversion = False, dim=1, square_potential = True,):
    '''
    Create a trimer potential profile of nxn patches. If n is odd, connecting patches ('0') are (n-2)x(n-2) and centered,
    if n is even, connecting patches are (n-1)x(n-1) and snap to the outer edges of the trimer.
    
     _0   _0
   0|1 |0|2 |0
     --   --
     0_    0
   0|3 |0
     --
     0
     
    :param kwant.builder.site site: a given site 
    :param float a: the effective site size
    :param float Vo: the potential surrounding the patches
    :param float V1: the potential on the connecting patches
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param array start_pos: the start position of the top left corner of the first patch
    :param bool in_chain_bulk: if the trimer is being called as part of a chain, we add connecting patches around the first patch.
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    
    '''
    
    #connecting patch size
    n_ = np.max([n-1-n%2,1])
    
    
    if break_inversion == False:
        n__ = False
        rectangle_12 = False
        rectangle_13 = False
    elif break_inversion:
        n__ = np.max([n_*a//2,1])
        if break_inversion == 'center_axis':
            rectangle_12 = n__
            rectangle_13 = False
        elif break_inversion == 'unit_cell':
            rectangle_12 = False
            rectangle_13 = n__
        else:
            raise ValueError('specify how the inversion symmetry will be broken: center_axis or unit_cell.')
    
    
    #first, we do trimer_n
    original_trimer = trimer_n(site,a,Vo,V1,n,start_pos, in_chain_bulk, break_inversion=break_inversion, dim=dim)
    if original_trimer != Vo:
        return original_trimer
    
    #then, the additional connecting patches
    #terminology for the name of the patches is from diamond_chain.py 
    b11 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+(n+n_)*a+1*a, start_pos[1]-n_*a],rectangle=rectangle_12, rectangle_y=True,dim=dim, square_potential = square_potential)
    if b11[0] == 0:
        return b11[1]
    
    b12 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+(2*n+n_)*a, start_pos[1]+1*a],dim=dim, square_potential = square_potential)
    if b12[0] == 0:
        return b12[1]
    
    c11 = patch_n(site,a, Vo, V1, n_, [start_pos[0]-n_*a, start_pos[1]+(n+n_)*a+1*a],dim=dim, square_potential = square_potential)
    if c11[0] == 0:
        return c11[1]
    
    c12 = patch_n(site,a, Vo, V1, n_, [start_pos[0]+1*a, start_pos[1]+(2*n+n_)*a],dim=dim, square_potential = square_potential)
    if c12[0] == 0:
        return c12[1]
    
    ca = patch_n(site,a, Vo, V1, n_, [start_pos[0]+(n+n_)*a+1*a, start_pos[1]+n*a],dim=dim, square_potential = square_potential)
    if ca[0] == 0:
        return ca[1]
    
    ba = patch_n(site,a, Vo, V1, n_, [start_pos[0]+n*a, start_pos[1]+(n+n_)*a+1*a],dim=dim, square_potential = square_potential)
    if ba[0] == 0:
        return ba[1]
        
    return Vo

def homogeneous_unit_cell_trimer_n(site,a,Vo,V1,n,start_pos,in_chain_bulk=False, break_inversion=False,dim=1, square_potential = True):
    '''
    
         o      o
        ___    ___
      o|   |oo|   |o
        - -    - -  
         o      o
         o   
        ___        etc.
      o|   |o    
        - -
         o
    
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param int Nc: the number of the unit cell; affects the start position. python counting (starts at 0).
    :param array start_pos: the start position of the top left corner of the chain
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    '''
    assert n%2 != 0
    
    #smaller patch size
    n_ = n//2
    
    a_patch = homogeneous_unit_cell_n(site,a,Vo,V1,n,start_pos,in_chain_bulk, break_inversion, dim, square_potential)
    if a_patch[0] == 0:
        return a_patch[1]
    b_patch = homogeneous_unit_cell_n(site,a,Vo,V1,n,[start_pos[0]+(2*n_+n)*a, start_pos[1]],in_chain_bulk, break_inversion, dim, square_potential)
    if b_patch[0] == 0:
        return b_patch[1]
    c_patch = homogeneous_unit_cell_n(site,a,Vo,V1,n,[start_pos[0], start_pos[1]+(2*n_+n)*a],in_chain_bulk, break_inversion, dim, square_potential)
    if c_patch[0] == 0:
        return c_patch[1]
    
    return Vo
    

def homogeneous_unit_cell_n(site,a,Vo,V1,n,start_pos,in_chain_bulk=False, break_inversion=False,dim=1, square_potential = True):
    '''
         o
        ___    
      o|   |o
        - -      
         o       
    
    
    '''
    assert n%2 != 0
    
    #smaller patch size
    n_ = n//2
    
    main_patch = patch_n(site,a, Vo, V1, n, start_pos, dim=dim, square_potential = square_potential)
    if main_patch[0] == 0:
        return main_patch
    top_patch = patch_n(site,a, Vo, V1, n_, [start_pos[0]+(n//2)*a, start_pos[1]-n_*a], dim=dim, square_potential = square_potential)
    if top_patch[0] == 0:
        return top_patch
    left_patch = patch_n(site,a, Vo, V1, n_, [start_pos[0]-n_*a, start_pos[1]+(n//2)*a], dim=dim, square_potential = square_potential)
    if left_patch[0] == 0:
        return left_patch
    bottom_patch = patch_n(site,a, Vo, V1, n_, [start_pos[0]+(n//2)*a, start_pos[1]+(n)*a], dim=dim, square_potential = square_potential)
    if bottom_patch[0] == 0:
        return bottom_patch
    right_patch = patch_n(site,a, Vo, V1, n_, [start_pos[0]+(n)*a, start_pos[1]+(n//2)*a], dim=dim, square_potential = square_potential)
    if right_patch[0] == 0:
        return right_patch
    
    return [Vo, Vo]
    



def quartic_trimer_1_n(site,a,Vo,V1,n,start_pos, in_chain_bulk=False, dim=1, with_embellishments=True, square_potential = True):
    '''
    Create a trimer potential profile of nxn patches. n must be odd in this case, since we have two different types of connecting patches, one small (n-3) and one big (n-2). Embellishments add additional connecting patches to the outside of the interstitial sites.
          _         _
         |_|       |_|
     _   _o_   _   _o_   _
    |_|o|   |o|_|o|   |o|_|
         - -       - -
          o         o
         |_|       |_|
     _   _o_   _   
    |_|o|   |o|_|
         - -      
          o
         |_|
     
     
    :param kwant.builder.site site: a given site 
    :param float a: the effective site size
    :param float Vo: the potential surrounding the patches
    :param float V1: the potential on the connecting patches
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param array start_pos: the start position of the top left corner of the first patch
    :param bool in_chain_bulk: if the trimer is being called as part of a chain, we add connecting patches around the first patch.
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    
    '''
    
    #connecting patch sizes
    big_n = n-2
    small_n = n-4
    
    
    #main sites A,B,C
    
    site_A = patch_n(site, a, Vo, V1, n, start_pos, square_potential)
    if site_A[0] == 0:
        return site_A[1]
    site_B = patch_n(site, a, Vo, V1, n, [start_pos[0] + (n+2*small_n+big_n)*a, start_pos[1]], square_potential = square_potential)
    if site_B[0] == 0:
        return site_B[1]
    site_C = patch_n(site, a, Vo, V1, n, [start_pos[0], start_pos[1] + (n+2*small_n+big_n)*a], square_potential = square_potential) #i'm assuming it;s plus here, since it seems to be the trend errywhere
    if site_C[0] == 0:
        return site_C[1]
    
    #interstitial sites ab, ba, ac, ca
    site_ab = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+small_n)*a, start_pos[1] + 1*a], square_potential = square_potential) 
    if site_ab[0] == 0:
        return site_ab[1]
    site_ac = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + 1*a, start_pos[1] + (n+small_n)*a], square_potential = square_potential) 
    if site_ac[0] == 0:
        return site_ac[1]
    site_ba = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+2*small_n+big_n+1)*a, start_pos[1] + (n+small_n)*a], square_potential = square_potential) 
    if site_ba[0] == 0:
        return site_ba[1]
    site_ca = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+small_n)*a, start_pos[1] + (n+2*small_n+big_n+1)*a], square_potential = square_potential) 
    if site_ca[0] == 0:
        return site_ca[1]
    
    # ba and ca sites that attach to first A site in chain
    if in_chain_bulk == False:
        site_ba_2 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + 1*a, start_pos[1]-(big_n+small_n)*a], square_potential = square_potential)
        if site_ba_2[0] == 0:
            return site_ba_2[1]
        site_ca_2 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] -(big_n+small_n)*a, start_pos[1] + 1*a], square_potential = square_potential)
        if site_ca_2[0] == 0:
            return site_ca_2[1]
        #also, connecting patches
        connector_ba_2_A = patch_n(site, a, V1, Vo, small_n, [start_pos[0] + 2*a, start_pos[1]-small_n*a], square_potential = square_potential)
        if connector_ba_2_A[0] == 0:
            return connector_ba_2_A[1]
        connector_ca_2_A = patch_n(site, a, V1, Vo, small_n, [start_pos[0] - small_n*a, start_pos[1] + 2*a], square_potential = square_potential)
        if connector_ca_2_A[0] == 0:
            return connector_ca_2_A[1]
        
    
    #additional sites b21, b22, c21, c22
    site_b21 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+2*small_n+big_n+1)*a, start_pos[1]-(small_n+big_n)*a], square_potential = square_potential)
    if site_b21[0] == 0:
        return site_b21[1]
    site_b22 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (2*n+3*small_n+big_n)*a, start_pos[1] + 1*a], square_potential = square_potential)
    if site_b22[0] == 0:
        return site_b22[1]
    site_c21 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + 1*a, start_pos[1] + (2*n+3*small_n+big_n)*a], square_potential = square_potential)
    if site_c21[0] == 0:
        return site_c21[1]
    site_c22 = patch_n(site, a, Vo, V1, big_n, [start_pos[0]-(small_n+big_n)*a, start_pos[1] + (n+2*small_n+big_n+1)*a], square_potential = square_potential)
    if site_c22[0] == 0:
        return site_c22[1]
    
    #connecting patches
    connector_A_ab = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + n*a, start_pos[1] + 2*a], square_potential = square_potential)
    if connector_A_ab[0] == 0:
        return connector_A_ab[1]
    connector_ab_B = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + small_n + big_n)*a, start_pos[1] + 2*a], square_potential = square_potential)
    if connector_ab_B[0] == 0:
        return connector_ab_B[1]
    connector_A_ac = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + n*a], square_potential = square_potential)
    if connector_A_ac[0] == 0:
        return connector_A_ac[1]
    connector_ac_C = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + (n + small_n + big_n)*a], square_potential = square_potential)
    if connector_ac_C[0] == 0:
        return connector_ac_C[1]
    connector_B_ba = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + 2*small_n + big_n + 2)*a, start_pos[1] + n*a ], square_potential = square_potential)
    if connector_B_ba[0] == 0:
        return connector_B_ba[1]
    connector_C_ca = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + n*a, start_pos[1] + (n + 2*small_n + big_n + 2)*a], square_potential = square_potential)
    if connector_C_ca[0] == 0:
        return connector_C_ca[1]
    connector_B_b21 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + 2*small_n + big_n+2)*a, start_pos[1] - (small_n)*a ], square_potential = square_potential)
    if connector_B_b21[0] == 0:
        return connector_B_b21[1]
    connector_B_b22 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n + 2*small_n + big_n)*a, start_pos[1] + 2*a ], square_potential = square_potential)
    if connector_B_b22[0] == 0:
        return connector_B_b22[1]
    connector_C_c21 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + (2*n + 2*small_n + big_n)*a], square_potential = square_potential)
    if connector_C_c21[0] == 0:
        return connector_C_c21[1]
    connector_C_c22 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] - small_n*a, start_pos[1] + (n + 2*small_n + big_n+2)*a], square_potential = square_potential)
    if connector_C_c22[0] == 0:
        return connector_C_c22[1]
    
    if with_embellishments:
        #have to add a bunch of extra smaller patches to the interstitial + external sites, in order to 'homogenize' them.
        #notation: we use north (N), south (S), east (E) and west (W) to denote which position the patch has on the smaller patches, in analogy to the cardinal directions of a compass. 
        #   n
        # w|_|e
        #   s
        
#         if in_chain_bulk == False:
        embellishment_ba_2_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1]-(2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ba_2_n[0] == 0:
            return embellishment_ba_2_n[1]
        embellishment_ba_2_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0], start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_ba_2_w[0] == 0:
            return embellishment_ba_2_w[1]
        embellishment_ba_2_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (big_n+1)*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_ba_2_e[0] == 0:
            return embellishment_ba_2_e[1]

        embellishment_ca_2_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(small_n+big_n//2+1)*a, start_pos[1]], square_potential = square_potential)
        if embellishment_ca_2_n[0] == 0:
            return embellishment_ca_2_n[1]
        embellishment_ca_2_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(2*small_n+big_n)*a, start_pos[1]+2*a], square_potential = square_potential)
        if embellishment_ca_2_w[0] == 0:
            return embellishment_ca_2_w[1]
        embellishment_ca_2_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(small_n+big_n//2+1)*a, start_pos[1]+(n-1)*a], square_potential = square_potential)
        if embellishment_ca_2_s[0] == 0:
            return embellishment_ca_2_s[1]
            
        #embellishments for ab, ac, ba, ca
        embellishment_ab_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]], square_potential = square_potential)
        if embellishment_ab_n[0] == 0:
            return embellishment_ab_n[1]
        embellishment_ab_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]+(small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ab_s[0] == 0:
            return embellishment_ab_s[1]
        embellishment_ac_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0], start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ac_w[0] == 0:
            return embellishment_ac_w[1]
        embellishment_ac_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(n-1)*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ac_e[0] == 0:
            return embellishment_ac_e[1]
        embellishment_ba_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(n+2*small_n+big_n)*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ba_w[0] == 0:
            return embellishment_ba_w[1]
        embellishment_ba_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(n+3*small_n+2*big_n)*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ba_e[0] == 0:
            return embellishment_ba_e[1]
        embellishment_ca_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]+(n+2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ca_n[0] == 0:
            return embellishment_ca_n[1]
        embellishment_ca_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]+(n+3*small_n+2*big_n)*a], square_potential = square_potential)
        if embellishment_ca_s[0] == 0:
            return embellishment_ca_s[1]
        
        #additional embellishments for the end of the chain
        embellishment_ba_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] +(n+2*small_n+big_n)*a + 2*a, start_pos[1]+(n+small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ba_s[0] == 0:
            return embellishment_ba_s[1]
        embellishment_ca_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + small_n + big_n)*a, start_pos[1]+(n+2*small_n+big_n+2)*a], square_potential = square_potential)
        if embellishment_ca_e[0] == 0:
            return embellishment_ca_e[1]
        
        #embellishments for b21, b22, c21, c22
        embellishment_b21_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a +(n+small_n*2+big_n)*a, start_pos[1]-(2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_b21_n[0] == 0:
            return embellishment_b21_n[1]
        embellishment_b21_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(n+small_n*2+big_n)*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_b21_w[0] == 0:
            return embellishment_b21_w[1]
        embellishment_b21_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (big_n+1)*a+(n+small_n*2+big_n)*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_b21_e[0] == 0:
            return embellishment_b21_e[1]
        
        embellishment_b22_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n+3*small_n+big_n+big_n//2)*a, start_pos[1]], square_potential = square_potential)
        if embellishment_b22_n[0] == 0:
            return embellishment_b22_n[1]
        embellishment_b22_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n+3*small_n+big_n+big_n)*a, start_pos[1]+2*a], square_potential = square_potential)
        if embellishment_b22_e[0] == 0:
            return embellishment_b22_e[1]
        embellishment_b22_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n+3*small_n+big_n+big_n//2)*a, start_pos[1]+(n-1)*a], square_potential = square_potential)
        if embellishment_b22_s[0] == 0:
            return embellishment_b22_s[1]
        
        
        embellishment_c21_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] - (small_n+big_n//2+1)*a, start_pos[1]+(n+2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_c21_n[0] == 0:
            return embellishment_c21_n[1]
        embellishment_c21_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(2*small_n+big_n)*a, start_pos[1]+2*a+(n+2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_c21_w[0] == 0:
            return embellishment_c21_w[1]
        embellishment_c21_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(small_n+big_n//2+1)*a, start_pos[1]+(n-1)*a+(n+2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_c21_s[0] == 0:
            return embellishment_c21_s[1]
        embellishment_c22_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0], start_pos[1] + (n+small_n+big_n//2)*a +(big_n-1+3*small_n+n)*a], square_potential = square_potential)
        if embellishment_c22_w[0] == 0:
            return embellishment_c22_w[1]
        embellishment_c22_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n-1)*a, start_pos[1] + (n+small_n+big_n//2)*a+(big_n-1+3*small_n+n)*a], square_potential = square_potential)
        if embellishment_c22_e[0] == 0:
            return embellishment_c22_e[1]
        embellishment_c22_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + (2*n+3*small_n+2*big_n)*a], square_potential = square_potential)
        if embellishment_c22_s[0] == 0:
            return embellishment_c22_s[1]
        

    return Vo






def quartic_trimer_2_n(site,a,Vo,V1,n,start_pos, in_chain_bulk=False, dim=1, with_embellishments=True, square_potential = True):
    '''
    Create a trimer potential profile of nxn patches. n must be odd in this case, since we have two different types of connecting patches, one small (n-3) and one big (n). Embellishments add additional connecting patches to the outside of the interstitial sites.
          _         _
         |_|       |_|
     _   _o_   _   _o_   _
    |_|o|   |o|_|o|   |o|_|
         - -       - -
          o         o
         |_|       |_|
     _   _o_   _   
    |_|o|   |o|_|
         - -      
          o
         |_|
     
     
    :param kwant.builder.site site: a given site 
    :param float a: the effective site size
    :param float Vo: the potential surrounding the patches
    :param float V1: the potential on the connecting patches
    :param int n: the size of the patches in units of the effective site size, min of 2
    :param array start_pos: the start position of the top left corner of the first patch
    :param bool in_chain_bulk: if the trimer is being called as part of a chain, we add connecting patches around the first patch.
    :param bool | str break_inversion: if 'center_axis', then connector 1-2 has a different size than connector 1-3. if 'unit_cell', vice-versa. If False, nothing happens.
    
    '''
    
    #connecting patch sizes
    big_n = n
    small_n = n-4
    
    
    #main sites A,B,C
    
    site_A = patch_n(site, a, Vo, V1, n, start_pos)
    if site_A[0] == 0:
        return site_A[1]
    site_B = patch_n(site, a, Vo, V1, n, [start_pos[0] + (n+2*small_n+big_n)*a, start_pos[1]], square_potential = square_potential)
    if site_B[0] == 0:
        return site_B[1]
    site_C = patch_n(site, a, Vo, V1, n, [start_pos[0], start_pos[1] + (n+2*small_n+big_n)*a], square_potential = square_potential) #i'm assuming it;s plus here, since it seems to be the trend errywhere
    if site_C[0] == 0:
        return site_C[1]
    
    #interstitial sites ab, ba, ac, ca
    site_ab = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+small_n)*a, start_pos[1] ], square_potential = square_potential) 
    if site_ab[0] == 0:
        return site_ab[1]
    site_ac = patch_n(site, a, Vo, V1, big_n, [start_pos[0] , start_pos[1] + (n+small_n)*a], square_potential = square_potential) 
    if site_ac[0] == 0:
        return site_ac[1]
    site_ba = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+2*small_n+big_n)*a, start_pos[1] + (n+small_n)*a], square_potential = square_potential) 
    if site_ba[0] == 0:
        return site_ba[1]
    site_ca = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+small_n)*a, start_pos[1] + (n+2*small_n+big_n)*a], square_potential = square_potential) 
    if site_ca[0] == 0:
        return site_ca[1]
    
    # ba and ca sites that attach to first A site in chain
    if in_chain_bulk == False:
        site_ba_2 = patch_n(site, a, Vo, V1, big_n, [start_pos[0], start_pos[1]-(big_n+small_n)*a], square_potential = square_potential)
        if site_ba_2[0] == 0:
            return site_ba_2[1]
        site_ca_2 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] -(big_n+small_n)*a, start_pos[1]], square_potential = square_potential)
        if site_ca_2[0] == 0:
            return site_ca_2[1]
        #also, connecting patches
        connector_ba_2_A = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1]-small_n*a], square_potential = square_potential)
        if connector_ba_2_A[0] == 0:
            return connector_ba_2_A[1]
        connector_ca_2_A = patch_n(site, a, Vo, V1, small_n, [start_pos[0] - small_n*a, start_pos[1] + 2*a], square_potential = square_potential)
        if connector_ca_2_A[0] == 0:
            return connector_ca_2_A[1]
        
    
    #additional sites b21, b22, c21, c22
    site_b21 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (n+2*small_n+big_n)*a, start_pos[1]-(small_n+big_n)*a], square_potential = square_potential)
    if site_b21[0] == 0:
        return site_b21[1]
    site_b22 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] + (2*n+3*small_n+big_n)*a, start_pos[1] ], square_potential = square_potential)
    if site_b22[0] == 0:
        return site_b22[1]
    site_c21 = patch_n(site, a, Vo, V1, big_n, [start_pos[0] , start_pos[1] + (2*n+3*small_n+big_n)*a], square_potential = square_potential)
    if site_c21[0] == 0:
        return site_c21[1]
    site_c22 = patch_n(site, a, Vo, V1, big_n, [start_pos[0]-(small_n+big_n)*a, start_pos[1] + (n+2*small_n+big_n)*a], square_potential = square_potential)
    if site_c22[0] == 0:
        return site_c22[1]
    
    #connecting patches
    connector_A_ab = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + n*a, start_pos[1] + 2*a], square_potential = square_potential)
    if connector_A_ab[0] == 0:
        return connector_A_ab[1]
    connector_ab_B = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + small_n + big_n)*a, start_pos[1] + 2*a], square_potential = square_potential)
    if connector_ab_B[0] == 0:
        return connector_ab_B[1]
    connector_A_ac = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + n*a], square_potential = square_potential)
    if connector_A_ac[0] == 0:
        return connector_A_ac[1]
    connector_ac_C = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + (n + small_n + big_n)*a], square_potential = square_potential)
    if connector_ac_C[0] == 0:
        return connector_ac_C[1]
    connector_B_ba = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + 2*small_n + big_n + 2)*a, start_pos[1] + n*a ], square_potential = square_potential)
    if connector_B_ba[0] == 0:
        return connector_B_ba[1]
    connector_C_ca = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + n*a, start_pos[1] + (n + 2*small_n + big_n + 2)*a], square_potential = square_potential)
    if connector_C_ca[0] == 0:
        return connector_C_ca[1]
    connector_B_b21 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + 2*small_n + big_n+2)*a, start_pos[1] - (small_n)*a ], square_potential = square_potential)
    if connector_B_b21[0] == 0:
        return connector_B_b21[1]
    connector_B_b22 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n + 2*small_n + big_n)*a, start_pos[1] + 2*a ], square_potential = square_potential)
    if connector_B_b22[0] == 0:
        return connector_B_b22[1]
    connector_C_c21 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + (2*n + 2*small_n + big_n)*a], square_potential = square_potential)
    if connector_C_c21[0] == 0:
        return connector_C_c21[1]
    connector_C_c22 = patch_n(site, a, Vo, V1, small_n, [start_pos[0] - small_n*a, start_pos[1] + (n + 2*small_n + big_n+2)*a], square_potential = square_potential)
    if connector_C_c22[0] == 0:
        return connector_C_c22[1]
    
    if with_embellishments:
        #have to add a bunch of extra smaller patches to the interstitial + external sites, in order to 'homogenize' them.
        #notation: we use north (N), south (S), east (E) and west (W) to denote which position the patch has on the smaller patches, in analogy to the cardinal directions of a compass. 
        #   n
        # w|_|e
        #   s
        
#         if in_chain_bulk == False:
        embellishment_ba_2_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1]-(2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ba_2_n[0] == 0:
            return embellishment_ba_2_n[1]
        embellishment_ba_2_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]-small_n*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_ba_2_w[0] == 0:
            return embellishment_ba_2_w[1]
        embellishment_ba_2_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (big_n)*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_ba_2_e[0] == 0:
            return embellishment_ba_2_e[1]

        embellishment_ca_2_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(small_n+big_n//2+1)*a, start_pos[1]-small_n*a], square_potential = square_potential)
        if embellishment_ca_2_n[0] == 0:
            return embellishment_ca_2_n[1]
        embellishment_ca_2_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(2*small_n+big_n)*a, start_pos[1]+2*a], square_potential = square_potential)
        if embellishment_ca_2_w[0] == 0:
            return embellishment_ca_2_w[1]
        embellishment_ca_2_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(small_n+big_n//2+1)*a, start_pos[1]+(n)*a], square_potential = square_potential)
        if embellishment_ca_2_s[0] == 0:
            return embellishment_ca_2_s[1]
            
        #embellishments for ab, ac, ba, ca
        embellishment_ab_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]-small_n*a], square_potential = square_potential)
        if embellishment_ab_n[0] == 0:
            return embellishment_ab_n[1]
        embellishment_ab_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]+(big_n)*a], square_potential = square_potential)
        if embellishment_ab_s[0] == 0:
            return embellishment_ab_s[1]
        embellishment_ac_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]-small_n*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ac_w[0] == 0:
            return embellishment_ac_w[1]
        embellishment_ac_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(n)*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ac_e[0] == 0:
            return embellishment_ac_e[1]
        embellishment_ba_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(2*n+small_n)*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ba_w[0] == 0:
            return embellishment_ba_w[1]
        embellishment_ba_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(2*n+2*small_n+big_n)*a, start_pos[1]+ (n+small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_ba_e[0] == 0:
            return embellishment_ba_e[1]
        embellishment_ca_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]+(2*n+small_n)*a], square_potential = square_potential)
        if embellishment_ca_n[0] == 0:
            return embellishment_ca_n[1]
        embellishment_ca_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n+small_n+big_n//2)*a, start_pos[1]+(2*n+2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ca_s[0] == 0:
            return embellishment_ca_s[1]
        
        #additional embellishments for the end of the chain
        embellishment_ba_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] +(n+2*small_n+big_n)*a + 2*a, start_pos[1]+(n+small_n+big_n)*a], square_potential = square_potential)
        if embellishment_ba_s[0] == 0:
            return embellishment_ba_s[1]
        embellishment_ca_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n + small_n + big_n)*a, start_pos[1]+(n+2*small_n+big_n+2)*a], square_potential = square_potential)
        if embellishment_ca_e[0] == 0:
            return embellishment_ca_e[1]
        
        #embellishments for b21, b22, c21, c22
        embellishment_b21_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n+2*small_n+2)*a, start_pos[1]-(2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_b21_n[0] == 0:
            return embellishment_b21_n[1]
        embellishment_b21_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]+(2*n+small_n)*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_b21_w[0] == 0:
            return embellishment_b21_w[1]
        embellishment_b21_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (3*n+2*small_n)*a, start_pos[1]-(small_n+big_n//2+1)*a], square_potential = square_potential) #halfway up the big_n patch
        if embellishment_b21_e[0] == 0:
            return embellishment_b21_e[1]
        
        embellishment_b22_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (3*n+3*small_n+big_n//2)*a, start_pos[1]-small_n*a], square_potential = square_potential)
        if embellishment_b22_n[0] == 0:
            return embellishment_b22_n[1]
        embellishment_b22_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n+3*small_n+big_n+big_n)*a, start_pos[1]+2*a], square_potential = square_potential)
        if embellishment_b22_e[0] == 0:
            return embellishment_b22_e[1]
        embellishment_b22_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (2*n+3*small_n+big_n+big_n//2)*a, start_pos[1]+(n)*a], square_potential = square_potential)
        if embellishment_b22_s[0] == 0:
            return embellishment_b22_s[1]
        
        
        embellishment_c21_n = patch_n(site, a, Vo, V1, small_n, [start_pos[0] - (small_n+big_n//2+1)*a, start_pos[1]+(2*n+small_n)*a], square_potential = square_potential)
        if embellishment_c21_n[0] == 0:
            return embellishment_c21_n[1]
        embellishment_c21_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(2*small_n+big_n)*a, start_pos[1]+2*a+(n+2*small_n+big_n)*a], square_potential = square_potential)
        if embellishment_c21_w[0] == 0:
            return embellishment_c21_w[1]
        embellishment_c21_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] -(small_n+big_n//2+1)*a, start_pos[1]+(3*n+2*small_n)*a], square_potential = square_potential)
        if embellishment_c21_s[0] == 0:
            return embellishment_c21_s[1]
        embellishment_c22_w = patch_n(site, a, Vo, V1, small_n, [start_pos[0]-small_n*a, start_pos[1] + (3*n+3*small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_c22_w[0] == 0:
            return embellishment_c22_w[1]
        embellishment_c22_e = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + (n)*a, start_pos[1] + (3*n+3*small_n+big_n//2)*a], square_potential = square_potential)
        if embellishment_c22_e[0] == 0:
            return embellishment_c22_e[1]
        embellishment_c22_s = patch_n(site, a, Vo, V1, small_n, [start_pos[0] + 2*a, start_pos[1] + (2*n+3*small_n+2*big_n)*a], square_potential = square_potential)
        if embellishment_c22_s[0] == 0:
            return embellishment_c22_s[1]
        

    return Vo


def patch_n(site, a, Vo, V1, n, start_pos, rectangle=False,rectangle_y=False,dim=1, main=False, square_potential = True):
    '''
    Make a patch of size nxn, unless it's by size nxrectangle, if rectangle is not False.
    
    :param bool | int rectangle: either False if the patch is square with size n, or an int different from n defining the second dimension of the patch.
    :param bool rectangle_y: whether the smaller dimension of the rectangle is in the x direction (True) or not (False)
    '''
    try:
        x, y = site.pos
#         x_ = x + -start_pos[0]
    except:
        x,y = site 
        
    if rectangle:
        n___ = rectangle
    else:
        n___ = n*a
        
    #shift origin to top left corner
    x_ = x - start_pos[0]
    y_ = y + start_pos[1]
    
    if rectangle_y:
        cond = (0 >= y_ >= -n*a and 0 <= x_ <= n___)
    else:
        cond = (0 <= x_ <= n*a and 0 >= y_ >= -n___)

    #disk shape
#     if main:
#         cond = np.sqrt((x-n*a/2)**2+(y+n___/2)**2)<=n*a/1.3
#     else:
#         cond = (0 <= x <= n*a and 0 >= y >= -n___)
    
    if cond:
        if square_potential:
            return [0,V1]
        else: #return slanted potential.
            if rectangle_y:
                return [0, V1+slanted_potential_profile(x_,y_, n___, n*a, Vo)]
            else:
                return [0, V1+slanted_potential_profile(x_,y_, n*a, n___, Vo)]
    else:
        return [Vo,Vo]
    

def slanted_potential_profile(x_,y_,l,w, Vo):
    #move origin to centre of patch
    
    x_ += -l/2
    y_ += w/2
    
    #quadratic decrease from Vo until we hit 0
    
    longest_dimension = np.max(np.array([l,w]))
    
    magnitude = ( ( longest_dimension - np.sqrt(x_**2 + y_**2)/longest_dimension )/longest_dimension )**2
    
    return np.max([Vo-magnitude, 0])
    
    
    

def onsite(site, Vo, V1, potential_type, a, n, start_pos, Nc, closed_chain, start_inversion_breaking_pos, break_inversion,trivial,dim,with_embellishments, square_potential):
    if potential_type == 'patch_n':
        return 4*t+patch_n(site, a, Vo, n, start_pos,dim=dim, square_potential = square_potential)
    elif potential_type == 'trimer_n':
        return 4*t+trimer_n(site, a, Vo,V1, n, start_pos,break_inversion,dim=dim, square_potential = square_potential)
    elif potential_type == 'quartic_trimer_1_n':
        return 4*t+quartic_trimer_1_n(site, a, Vo,V1, n, start_pos,break_inversion,dim=dim, with_embellishments=with_embellishments, square_potential = square_potential)
    elif potential_type == 'quartic_trimer_2_n':
        return 4*t+quartic_trimer_2_n(site, a, Vo,V1, n, start_pos,break_inversion,dim=dim, with_embellishments=with_embellishments, square_potential = square_potential)
    elif potential_type == 'chain_n':
        return 4*t+chain_n(site, n, Nc, a, Vo,V1, start_pos, closed_chain, break_inversion,dim=dim, square_potential = square_potential)
    elif potential_type == 'homogeneous_chain_n':
        return homogeneous_chain_n(site, n, Nc, a, Vo,V1, start_pos, closed_chain, break_inversion,dim=dim, square_potential = square_potential)
    elif potential_type == 'quartic_chain_1_n':
        return 4*t+quartic_chain_1_n(site, n, Nc, a, Vo,V1, start_pos, closed_chain, dim=dim, with_embellishments=with_embellishments, square_potential = square_potential)
    elif potential_type == 'quartic_chain_2_n':
        return 4*t+quartic_chain_2_n(site, n, Nc, a, Vo,V1, start_pos, closed_chain, dim=dim, with_embellishments=with_embellishments, square_potential = square_potential)
    elif potential_type == 'homogeneous_unit_cell_n':
        return 4*t+homogeneous_unit_cell_n(site,a,Vo,V1,n,start_pos,break_inversion=break_inversion,dim=dim, square_potential = square_potential)
    elif potential_type == 'unit_cell_homogeneous_chain_n':
        return 4*t+unit_cell_homogeneous_chain_n(site, n, Nc, a, Vo, V1, start_pos, closed_chain, start_inversion_breaking_pos, break_inversion=break_inversion,  dim=dim, square_potential = square_potential)
    else:
        raise ValueError('unsupported potential type')
    
    
def potential_system(L,W,params,dim,cross_hop_x, cross_hop_y,semi_infinite=False, t=t):
    '''
    :param float t: the hopping strength
    :param float L: length of substrate
    :param float W: width of substrate
    :param float lattice_spacing: lattice spacing of the substrate
    :param list of dicts potential: patch or chain to be created 
    '''
    
    pot_type = params['potential_type']
    
    x_space = range(L)
    y_space = np.linspace(0,-W,W,endpoint=True)
    def y_cond(y):
        return y>0 or y<-W
    vect = [1,-1]
    
    if semi_infinite:
        
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry(vect))
    else:
        syst = kwant.Builder() #initializes the Hamiltonian
    
    if dim == 1:
        lat = kwant.lattice.square(1,norbs=1) #builds the lattice
        syst[(lat(i, j) for i in x_space for j in y_space)] = onsite #equates the diagonal elements of the Hamiltonian to the potential
        syst[lat.neighbors(n=1)] = t
                    
    else:
        raise valueError('dimension is not 1!')

    syst = syst.finalized() #hamiltonian is built

    return syst


def sorted_evs(syst, potential, k=15):
    '''
    :param kwant.Builder syst: kwant system of substrate
    '''
    
    ham_mat = syst.hamiltonian_submatrix(params=potential, sparse=True)
    evals, evecs = sla.eigsh(ham_mat.tocsc(), k=k, sigma=0, return_eigenvectors=True)
    
    evals = evals*27.211324570273+4.7
    inds = evals.argsort()
    sorted_evals = evals[inds]
    sorted_evecs = evecs[:,inds]
    return sorted_evals, sorted_evecs


def plotter(X,Y,sorted_evecs,sorted_evals,L,W, aspect = None,rounding=2, return_colormeshes=False):
    
    column_no = 8
    row_no = len(sorted_evals)//8+1
    
    fig = plt.figure(figsize=(column_no*2,row_no*2))
    gs = gridspec.GridSpec(
        ncols=column_no,
        nrows=row_no,
        width_ratios=[1 for i in range(column_no)],
        height_ratios=[1 for i in range(row_no)],
        hspace=0.2
    )
    

    j=0
    colormeshes = []
    for row in range(row_no):
        for i in range(column_no):
            try:
                b = sorted_evals[j+1]
            except:
                continue
                
            ax = plt.subplot(gs[row,i])
            
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_ylabel('')
            ax.set_xlabel('')
                
            
            k = 0
            while j+k+1 < len(sorted_evals) and round(sorted_evals[j+k],rounding) == round(sorted_evals[j+1+k],rounding):
                k += 1
                
            colormesh_ = np.zeros((L,W)).T
            for k_i in range(k+1):
                colormesh_+= np.abs(sorted_evecs[:,j+k_i].reshape((L, W)).T)**2
                
            colormeshes.append(colormesh_)
            ax.pcolormesh(X, Y, colormesh_, shading='auto', cmap='viridis')
            ax.set_title(str(round(sorted_evals[j],3))+'eV')
                
            j+=k+1
                
    return [None, colormeshes][return_colormeshes == True]


def view_potential_shape(L,W,params,dim,cross_hop_x, cross_hop_y,semi_infinite=False):
    '''
    :param float L: length of substrate
    :param float W: width of substrate
    :param float lattice_spacing: lattice spacing of the substrate
    :param list of dicts potential: patch or chain to be created 
    '''
    
    pot_type = params['potential_type']
    
    x_space = range(L)
    y_space = np.linspace(0,-W,W,endpoint=True)
    def y_cond(y):
        return y>0 or y<-W
    vect = [1,-1]
    
    if semi_infinite:
        
        syst = kwant.Builder(symmetry=kwant.lattice.TranslationalSymmetry(vect))
    else:
        syst = kwant.Builder() #initializes the Hamiltonian
    
    if dim == 1:
        
        def shape_func(pos):
            x,y = pos
            if x<0 or x> x_space[-1] or y_cond(y):
                return False
            else:
                if params['potential_type'] == 'trimer_n':
                    res = trimer_n(pos,
                                   a=params['a'],
                                   Vo=params['Vo'],
                                   V1=params['V1'],
                                   n=params['n'],
                                   start_pos=params['start_pos'], 
                                   in_chain_bulk=False,
                                   break_inversion=params['break_inversion'],
                                   dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'homogeneous_trimer_n':
                    res = homogeneous_trimer_n(pos,
                                   a=params['a'],
                                   Vo=params['Vo'],
                                   V1=params['V1'],
                                   n=params['n'],
                                   start_pos=params['start_pos'], 
                                   in_chain_bulk=params['in_chain_bulk'],
                                   break_inversion=params['break_inversion'],
                                   dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'quartic_trimer_1_n':
                    res = quartic_trimer_1_n(pos,
                                   a=params['a'],
                                   Vo=params['Vo'],
                                   V1=params['V1'],
                                   n=params['n'],
                                   start_pos=params['start_pos'], 
                                   in_chain_bulk=params['in_chain_bulk'],
                                   with_embellishments = params['with_embellishments'],
                                   dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'quartic_trimer_2_n':
                    res = quartic_trimer_2_n(pos,
                                   a=params['a'],
                                   Vo=params['Vo'],
                                   V1=params['V1'],
                                   n=params['n'],
                                   start_pos=params['start_pos'], 
                                   in_chain_bulk=params['in_chain_bulk'],
                                   with_embellishments = params['with_embellishments'],
                                   dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'chain_n':
                    res = chain_n(pos, 
                                  n=params['n'], 
                                  Nc=params['Nc'], 
                                  a=params['a'], 
                                  Vo=params['Vo'],
                                  V1=params['V1'], 
                                  start_pos=params['start_pos'], 
                                  closed_chain=params['closed_chain'],
                                  break_inversion=params['break_inversion'],
                                  dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'homogeneous_chain_n':
                    res = homogeneous_chain_n(pos, 
                                  n=params['n'], 
                                  Nc=params['Nc'], 
                                  a=params['a'], 
                                  Vo=params['Vo'],
                                  V1=params['V1'], 
                                  start_pos=params['start_pos'], 
                                  closed_chain=params['closed_chain'],
                                  break_inversion=params['break_inversion'],
                                  start_inversion_breaking_pos = params['start_inversion_breaking_pos'],
                                  dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'quartic_chain_1_n':
                    res = quartic_chain_1_n(pos, 
                                  n=params['n'], 
                                  Nc=params['Nc'], 
                                  a=params['a'], 
                                  Vo=params['Vo'],
                                  V1=params['V1'], 
                                  start_pos=params['start_pos'], 
                                  closed_chain=params['closed_chain'],
                                  with_embellishments = params['with_embellishments'],
                                  dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'quartic_chain_2_n':
                    res = quartic_chain_2_n(pos, 
                                  n=params['n'], 
                                  Nc=params['Nc'], 
                                  a=params['a'], 
                                  Vo=params['Vo'],
                                  V1=params['V1'], 
                                  start_pos=params['start_pos'], 
                                  closed_chain=params['closed_chain'],
                                  with_embellishments = params['with_embellishments'],
                                  dim=1,
                                   square_potential = params['square_potential'],
                                  )
                elif params['potential_type'] == 'homogeneous_unit_cell_trimer_n':
                    res = homogeneous_unit_cell_trimer_n(pos,
                                  n=params['n'], 
                                  a=params['a'], 
                                  Vo=params['Vo'],
                                  V1=params['V1'], 
                                  start_pos=params['start_pos'], 
                                  dim=1,
                                   square_potential = params['square_potential'],
                    )
                elif params['potential_type'] == 'unit_cell_homogeneous_chain_n':
                    res = unit_cell_homogeneous_chain_n(pos,
                                  n=params['n'], 
                                  Nc=params['Nc'], 
                                  a=params['a'], 
                                  Vo=params['Vo'],
                                  V1=params['V1'], 
                                  start_pos=params['start_pos'], 
                                  dim=1,
                                  closed_chain=params['closed_chain'],
                                  start_inversion_breaking_pos = params['start_inversion_breaking_pos'],
                                   square_potential = params['square_potential'],
                    )
                else:
                    raise ValueError('potential type not supported.')

                if not np.allclose(res,np.eye(dim)*params['Vo']):
                        return True
                else:
                    return False
    
        lat = kwant.lattice.square(1,norbs=1) #builds the lattice
        syst[lat.shape(shape_func, (params['start_pos'][0],-params['start_pos'][1]) )] = np.eye(dim)*0
        syst[lat.neighbors(n=1)] = params['t']
                    
    else:
        raise valueError('dimension is not 1!')

    syst = syst.finalized() #hamiltonian is built

    return syst