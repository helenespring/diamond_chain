# -*- coding: utf-8 -*-
# +
import kwant
import sympy
import plotly
import scipy

import numpy as np
import matplotlib.pyplot as plt
import kwant.qsymm as kwant_qsymm

import modules.diamond_chain as dc
from modules.analysis_funcs import ldos_from_kpm, ldos_modes_from_leads

import warnings
warnings.filterwarnings('ignore')
print('Warning that warnings are being ignored!')

import holoviews as hv
from holoviews import opts

hv.extension('matplotlib')


# -


# # Diamond chain

# +
E1 = 5.26
E2 = 5.23
t1 = 1.6
t2 = -0.01

N_c = 4

# system_params = dict(
#     mu_a_pos = E2,
#     mu_a_neg = E2,
#     mu_b_pos = E2,
#     mu_b_neg = E2,
#     mu_c_pos = E2,
#     mu_c_neg = E2,
#     j2 = t1,
#     j3 = t1,
#     phi = np.pi,
#     phi_d = 0,
#     b = -0.,
#     N_c = N_c,
# )

system_params = dict(
    mu_a = E2,
    mu_b = E2,
    mu_c = E2,
    j2 = t1,
    phi_d = 0,
    b=0.,
    N_c = N_c,
)

# +
closed_chain = False
broken_inversion = False

# inf_chain = dc.tilted_diamond_chain_system(system_params, semi_infinite = True, closed_chain=closed_chain,broken_inversion=broken_inversion,)
# fin_chain = dc.tilted_diamond_chain_system(system_params, semi_infinite = False, closed_chain=closed_chain,broken_inversion=broken_inversion)
# inf_chain = dc.quartic_ti(N_uc, system_params, semi_infinite=True,orbitals=s_or_p)
# fin_chain = dc.quartic_ti(N_uc, system_params, semi_infinite=False,orbitals=s_or_p)
inf_chain = dc.s_tilted_diamond_chain_system(system_params, semi_infinite = True, closed_chain=closed_chain,broken_inversion=broken_inversion)
fin_chain = dc.s_tilted_diamond_chain_system(system_params, semi_infinite = False, closed_chain=closed_chain,broken_inversion=broken_inversion)
# o = kwant.plot(fin_chain)
# -

# ## Spectrum

p = kwant.plotter.bands(inf_chain.finalized(), momenta = np.linspace(-np.pi,np.pi,1000))

# ## Eigenvalues

ham = fin_chain.finalized().hamiltonian_submatrix()
ev, es = np.linalg.eigh(ham)
plt.plot(ev, marker='o',lw=0)


# ## LDOS

# +
# ind = 4
# print(ev[ind])
# o = ldos_from_kpm(fin_chain.finalized(), system_params, vmax = 10, energy = ev[ind] )

ham = fin_chain.finalized().hamiltonian_submatrix()
ev, es = scipy.linalg.eig(ham)

zipped_eves = zip(ev,np.arange(0,ev.shape[0]))
sorted_zipped_eves = sorted(zipped_eves)
sorted_ev = np.array([ev_ for ev_, _ in sorted_zipped_eves])
sorted_inds = [e_ind for _, e_ind in sorted_zipped_eves]
sorted_es = np.array([es[:,e_ind] for e_ind in sorted_inds])


ldos = [] 
current_energy = sorted_ev[0]
current_ldos = np.abs(sorted_es[0])
rounding = 2
squashed_energies = [ sorted_ev[0] ]

for energy_ind in range(1, sorted_ev.shape[0]):
    
    energy = sorted_ev[energy_ind]
    es_ = np.abs(sorted_es[energy_ind])
    if np.round(current_energy,rounding) == np.round(energy,rounding):
        current_ldos += np.real(es_)
    else:
        squashed_energies.append(energy)
        ldos.append(current_ldos)
        current_ldos = es_
        current_energy = sorted_ev[energy_ind]
        
#add in the last one
ldos.append(current_ldos)


# +
ind = 0

print(np.real(squashed_energies[ind]))

kwant.plotter.density(
                fin_chain.finalized(), np.real(ldos[ind]), vmin=0
            );
# -

# ## Symmetries

# +
symmetries = kwant_qsymm.find_builder_symmetries(inf_chain, params=system_params, sparse=True,)

symmetries
# -
ind = 1
sympy.Matrix(np.round(symmetries[ind].U@np.conj(symmetries[ind].U),3))




# # Including orbital overlap

# +
N_c = 4

E1 = 5.26
E2 = 5.165 # 5.23
t1 = 1.6 #1.6
t2 = -0.01

s = 0.3095

E2_ = E2
t1_ = t1#0.01

# #tilted_chain (for p)
# system_params = dict(
#     mu_a_pos = E2_,
#     mu_a_neg = E2_,
#     mu_b_pos = E2_,
#     mu_b_neg = E2_,
#     mu_c_pos = E2_,
#     mu_c_neg = E2_,
#     j2=t1_,
#     j3=t1_,
#     phi = np.pi,
#     phi_d = 0,
#     b = -0.,
#     N_c = N_c,
# )

#s_tilted_chain
system_params = dict(
    mu_a = E2_,
    mu_b = E2_,
    mu_c = E2_,
    j2 = t1_,
    phi_d = 0,
    b=0., 
    N_c = N_c,
)

model_parameters = dict(
    system_params = system_params,
    semi_infinite=False,
#     closed_chain=False,
#     broken_inversion = False,
)

params = dict(
    model_params = model_parameters,
    orbital_overlap = s,
)


method = dc.s_tilted_diamond_chain_system 


ev, es = dc.chain_with_overlap(method, params)
ev = np.real(ev)
es = np.real(es)


# zipped_eves = zip(ev,np.transpose(es))
# sorted_zipped_eves = sorted(zipped_eves)

zipped_eves = zip(ev,np.arange(0,ev.shape[0]))
sorted_zipped_eves = sorted(zipped_eves)
sorted_ev = np.array([ev_ for ev_, _ in sorted_zipped_eves])
sorted_es = np.array([es[:,e_ind] for _, e_ind in sorted_zipped_eves])

ymin = sorted_ev[0]
ymax = sorted_ev[-1]

plt.figure()
plt.plot(sorted_ev, lw=0, marker='o')
# plt.axhline(2.03, c='red',ls=':')
# plt.axhline(3.6559501, c='red',ls=':')
plt.ylim(5,5.2)
# plt.ylim(0,10)

# IIPR = []
# for e_ind in range(sorted_ev.shape[0]):
#     sub_iipr = []
#     eigenvector = sorted_es[:,e_ind]
#     treated_entries = []
#     for entry in eigenvector:
#         treated_entries.append(np.abs(np.conj(entry) * entry) ** 4)
#     IIPR.append(np.sum(np.array(treated_entries)))
    
# IIPR = np.array(IIPR)
# IIPR /= np.max(IIPR)

# plt.figure()
# for ind, sev in enumerate(sorted_ev):
#     plt.scatter(ind, sev, alpha=IIPR[ind]
#                 ,c='black')
# plt.ylim(0,10)


# -

# ## Eigenvalues

# +
overlaps = np.linspace(0,0.5,30)
curves = dict()
for s in overlaps:
    params_ = params.copy()
    params_['orbital_overlap'] = s
    curves[s] = hv.Scatter(sorted( dc.chain_with_overlap(method, params_)[0] ), extents = (None,0,None,10))
#     curves[s] = hv.Scatter(dc.chain_with_overlap(method, params_)[0], extents = (None,0,None,10))


hv.HoloMap(curves, kdims = 'orbital overlap').opts(framewise=True, aspect=1, fig_size=100,)
# -

# ## DOS

# The ordering of the basis is kind of weird. It's $(B_{11},B_{21},B_{31}, ... B_{N_c1},A_{11},A_{21},...A_{N_c1},C_{11},...,C_{N_c1}, B_{12}, ...)$, instead of $(A_{11},A_{12},B_{11},B_{12},C_{11},C_{12},A_{21},A_{22},B_{21},B_{22},C_{21},...C_{N_c2})$, (grouped by unit cell) so have to take that into account when assigning eigenvector indices to the appropriate sites.
#
# Luckily it appears as though the site ordering follows the same scheme! so that's great.

# +
kwant_lattice = method(**model_parameters)

ldos = [] 
current_energy = sorted_ev[0]
current_ldos = np.abs(sorted_es[0])
rounding = 2
squashed_energies = [ sorted_ev[0] ]
squashed_eigenvectors = []

for energy_ind in range(1, sorted_ev.shape[0]):
    
    energy = sorted_ev[energy_ind]
    es_ = np.abs(sorted_es[energy_ind])
    if np.round(current_energy,rounding) == np.round(energy,rounding):
        current_ldos += np.real(es_)
    else:
        squashed_energies.append(energy)
        ldos.append(current_ldos)
        squashed_eigenvectors.append(current_ldos)
        current_ldos = es_
        current_energy = sorted_ev[energy_ind]
        
#add in the last one
ldos.append(current_ldos)
squashed_eigenvectors.append(current_ldos)

squashed_energies = np.array(squashed_energies)
squashed_eigenvectors = np.array(squashed_eigenvectors)

# +
ind = 0

print(np.real(squashed_energies[ind]))

kwant.plotter.density(
                kwant_lattice.finalized(), np.real(ldos[ind]), vmin=0, #vmax=6
            );
# -



