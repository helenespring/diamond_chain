# +
import kwant
import scipy.sparse.linalg as sla
import numpy as np
import matplotlib.gridspec as gridspec
import astropy


import modules.basic_potential as basic_potential
import modules.analysis_funcs as analysis_funcs

from modules.analysis_funcs import ldos_from_kpm
from matplotlib import pyplot as plt
# -

# # View potentials

# +
hbar = 1
m = 1
a = 6 #0.36 nm (lattice constant) in a.u.



lattice_spacing = 1
n = 3
Nc = 4

# big_n = n
# small_n = n-4

big_n = 3
small_n = 1

L = int( (n + big_n + 2*small_n)*a*(Nc+1)+big_n*a+2*small_n*a +1 )
W = L#sets the size of the discretized lattice (number points)
chain = dict(
    L=L,
    W=W,
    n=n,
    a = a,
#     start_pos=[(big_n+2*small_n)*a,(big_n+2*small_n)*a],
    start_pos=[6,6],
    Nc = Nc,
    Vo = 20,
    V1=0.,
    dim=1,
    closed_chain=False,
    trivial=False,
    break_inversion=False,#'center_axis',
    start_inversion_breaking_pos = 3,
    potential_type = 'homogeneous_chain_n',
    with_embellishments = False, #to 'homogenize' the quartic TI chain.
    in_chain_bulk = False,
    cross_hop_x = 0,
    cross_hop_y = 0,
    intra_hop=0.,
    square_potential = False,
)
# -

o = basic_potential.view_potential_shape(L=chain['L'],
                            W=chain['W'],
                            params=chain,
                            dim=chain['dim'],
                            cross_hop_x=0,
                            cross_hop_y=0,
                            semi_infinite=False,
                                           )
# o = basic_potential.potential_system(L,W,chain,dim=chain['dim'],cross_hop_x=chain['cross_hop_x'],cross_hop_y=chain['cross_hop_y'],)
p=kwant.plot(o, site_size=0.1,hop_lw=0.1, fig_size=(10,10))


# # LDOS + bands

# ## spectral function

fin_chain = basic_potential.potential_system(L=chain['L'],W=chain['W'],params=chain,dim=chain['dim'],
                                                 cross_hop_x=chain['cross_hop_x'],cross_hop_y=chain['cross_hop_y'],semi_infinite=False)

# +
energies = np.linspace(0, 4, 1001)
d = 0

# bulk window
window = lambda pos: (
    np.abs(pos[0]) <= chain['L'] - d and np.abs(pos[1]) <= np.abs(chain['W']) - d
)


# from Daniel's code
# momentum-resolved spectral function
def job(k):
    sf = potentials.spectral_function(
        fin_chain,
        k * np.array([1, 1]),
        params=chain,
        window=window,
        num_moments=1000,
    )
    return sf(energies)

ks = np.linspace(-3.14,3.14,100)
res = list(map(job, ks))

# from Daniel's code
spec_func = np.real(np.array(res).T)

# +

plt.figure(figsize=(5, 5))
plt.imshow(
    spec_func,
    vmin=0,
    vmax=1,
    extent=(ks[0], ks[-1], energies[0], energies[-1]),
    origin="lower",
    cmap='viridis'
)

# -

# ## Band structure

# inf_chain = basic_potential.view_potential_shape(L=chain['L'],
#                             W=chain['W'],
#                             params=chain,
#                             dim=chain['dim'],
#                             cross_hop_x=0,
#                             cross_hop_y=0,
#                             semi_infinite=True,
#                                            )
inf_chain = basic_potential.potential_system(L=chain['L'],W=chain['W'],params=chain,dim=chain['dim'],
                                                 cross_hop_x=chain['cross_hop_x'],cross_hop_y=chain['cross_hop_y'],semi_infinite=True)

# +
# kwant.plot(inf_chain)
# -

# p = kwant.plotter.bands(inf_chain.finalized(), momenta = np.linspace(-np.pi,np.pi,1000),params=chain)
p = kwant.plotter.bands(inf_chain, momenta = np.linspace(-np.pi,np.pi,1000),params=chain,)

# ## LDOS + evs w/ spectral index

fin_chain = potentials.view_potential_shape(L=chain['L']
                            ,W=chain['W'],
                            n=chain['n'],
                            Nc=chain['Nc'],
                            a=chain['a'],
                            Vo=chain['Vo'],
                            view=False,
                            potential_type='straight_chain',
                            dim=chain['dim'],
                            semi_infinite=False,
                                            cross_coupling_x=chain['cross_hop_x'],
                                            cross_coupling_y=chain['cross_hop_y'],
                                           )
fin_chain = fin_chain.finalized()
fin_chain = potentials.potential_system(L,W,chain,dim=chain['dim'],cross_hop_x=chain['cross_hop_x'],cross_hop_y=chain['cross_hop_y'],)
ev=None

o = ldos_from_kpm(fin_chain[0], chain, vmax = 10, energy = 2,norbs=1)


# ham = fin_chain.finalized().hamiltonian_submatrix(params=chain)
ham = fin_chain[0].hamiltonian_submatrix(params=chain)


ev, es = np.linalg.eigh(ham)
# sla.eigsh(ham_mat.tocsc(), sigma=0, return_eigenvectors=True)

plt.plot(ev, marker='o',lw=0,ms=1)
# plt.ylim((-1,10))

ev[0:20]


# # Chains

# +
a = 6 #0.36 nm (lattice constant) in a.u.

E2 = 1.993
t = 1.6

lattice_spacing = 1
n = 3
Nc = 4

big_n = n
small_n = n-4


L = 200 #int( (n + big_n + 2*small_n)*a*(Nc+1)+big_n*a+2*small_n*a +1 )
W = L#sets the size of the discretized lattice (number points)
chain = dict(
    L=L,
    W=W,
    n=n,
    a = a,
    start_pos=[n*a,n*a],
    Nc = Nc,
    Vo = 2.2,
    V1=E2,
    t = 1.6,
    dim=1,
    closed_chain=False,
    trivial=False,
    break_inversion=False,#'center_axis',
    start_inversion_breaking_pos = 3,
    potential_type = 'homogeneous_chain_n',
    with_embellishments = True, #to 'homogenize' the quartic TI chain.
    in_chain_bulk = False,
    cross_hop_x = 0,
    cross_hop_y = 0,
    intra_hop=0.,
    square_potential = True,
)

# +
syst= basic_potential.potential_system(L,W,chain, dim=chain['dim'], cross_hop_x=chain['cross_hop_x'],cross_hop_y=chain['cross_hop_y'])

sorted_evals, sorted_evecs = basic_potential.sorted_evs(syst, chain,k=40)
# -

plt.plot(sorted_evals, marker='o',lw=0,ms=1)
# plt.ylim((5.9, 6.2)) 
# plt.ylim((5.5, 5.8)) 
# plt.xlim((-1,15))

x, y = np.linspace(0,L,L,endpoint=False), np.linspace(0, -W, W, endpoint=False)
X, Y = np.meshgrid(x, y)
rounding = 2
if chain['dim'] == 1:
    colormeshes = basic_potential.plotter(X,Y,sorted_evecs,sorted_evals,int(L),int(W),rounding=rounding, return_colormeshes=True)
elif chain['dim'] == 2:
    colormeshes = basic_potential.plotter(X,Y,sorted_evecs[len(sorted_evecs)//2:,:],sorted_evals,int(L),int(W),rounding=rounding)

# +
# plt.figure(figsize=(4,4))
# plt.pcolormesh(X, Y, colormeshes[5], shading='auto', cmap='RdYlBu_r')
# plt.title('5.996 eV')
# plt.xticks([])
# plt.yticks([])
# plt.savefig('p_0_RdYlBu', format='png')
# -
points = analysis_funcs.point_specs([[12,-106],[12,-85], [35,-85]
                     ],
                     2,
                     colormeshes, sorted_evals, L=L, W=W, 
                     rounding = 2,
                     lorentzian_width = 0.01,
                     lorentzian_energy_precision=1000,
                     s_lims_in_x = (5.1, 5.3),
                     p_lims_in_x = (5.8, 6.2),
                                   )


